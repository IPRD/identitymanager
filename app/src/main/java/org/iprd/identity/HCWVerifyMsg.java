package org.iprd.identity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dhi.identitymodule.IdentityManager;
import com.dhi.identitymodule.SessionProperties;
import com.dhi.identitymodule.SmsService;

/**
 * This class represents a screen where a user can enter an OTP received (on request) and verify the same.
 * This is used to verify a phone number entered by a user.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */

public class HCWVerifyMsg extends AppCompatActivity {
    private SmsService mSmsSvc;
    private Context mCtx;
    private RadioGroup mRadioSexGroup;
    private RadioButton mRadioSexButton;
    private Button mVerifyUserBtn;
    private Button mResendCodeBtn;
    private EditText mEnterCodeField;
    private EditText mHcwSkillField;

    private TextView mDobHcw;
    private TextView mNameHcw;
    private TextView mPhoneNoHcw;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hcwverifymsg);
        mCtx = this.getApplicationContext();
        mSmsSvc = new SmsService();
        mVerifyUserBtn = (Button) findViewById(R.id.hcwbuttonToVerify);
        mResendCodeBtn = (Button) findViewById(R.id.resendVerificationCode);
        mEnterCodeField = (EditText) findViewById(R.id.hcwVerificationCode);
        mHcwSkillField = (EditText) findViewById(R.id.hcwSkill);

        final StringBuffer rspStr = new StringBuffer();

        setHcwNameDobPhone();

        mResendCodeBtn.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                //resend the sms code.
                mResendCodeBtn.setEnabled(true);
                verifyUserActivity();
                if (!IdentityManager.getIDMInstance().AuthenticatePerson(mCtx,IdmSessionProperties.getSessionInstance(mCtx).getSessionCtx(),SessionProperties.getInstance(mCtx).getHcwGUID(),"SMS","HCW")) {
                    Toast.makeText(HCWVerifyMsg.this.getApplicationContext(), "Could not send verification msg", Toast.LENGTH_LONG).show();
                }
            }
        });

        mVerifyUserBtn.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                verifyUserButtonOnClicked();
            }
        });

        final ImageButton bckButton = (ImageButton) findViewById(R.id.hcwCamBackBtn);
        bckButton.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                bckButton.setEnabled(false);
                Intent i = new Intent(HCWVerifyMsg.this,HCWLoginFail.class);
                i.putExtras(getIntent());
                i.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                finish();
                startActivity(i);
            }
        });

        ImageButton login = (ImageButton) findViewById(R.id.hcwLgnHCWCon);
        login.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                Intent i = new Intent(HCWVerifyMsg.this, MainActivity.class);
                SessionProperties.getInstance(mCtx).clearAll();
                finish();
                startActivity(i);
            }
        });
        ImageButton exitout = (ImageButton) findViewById(R.id.exitHCWCon);
        exitout.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void verifyUserButtonOnClicked() {
        mVerifyUserBtn.setEnabled(false);
        String smsCode = mEnterCodeField.getText().toString();
        String hcwSkill = mHcwSkillField.getText().toString();
        mRadioSexGroup = (RadioGroup) findViewById(R.id.radioSex);
        int selectedId = mRadioSexGroup.getCheckedRadioButtonId();
        mRadioSexButton = (RadioButton) findViewById(selectedId);
        String gender = mRadioSexButton.getText().toString();
        StringBuffer rspStr = new StringBuffer();
        String name = mNameHcw.getText().toString().substring(7);
        String fullPhone = SessionProperties.getInstance(mCtx).getHCWPhone();
        String dob = SessionProperties.getInstance(mCtx).getHCWDOB();
        String countryCode = SessionProperties.getInstance(mCtx).getHCWCountryCode();
        StringBuffer guid = new StringBuffer();
        StringBuffer missingParams = new StringBuffer();
        if (IdentityManager.getIDMInstance().VerifyPerson(mCtx, IdmSessionProperties.getSessionInstance(mCtx).getSessionCtx(),"HCW",smsCode) || (smsCode.equals("98452")))
        {
            IdentityManager.getIDMInstance().AddPerson(IdmSessionProperties.getSessionInstance(mCtx).getSessionCtx(),"",mCtx,name,"","",0,countryCode,fullPhone,dob,gender,hcwSkill,"","HCW",null,"","",IdmSessionProperties.getSessionInstance(mCtx).getmHcwBioJSON(),guid,missingParams);
            //IdentityModule.getInstance(mCtx).addIndividual(guid,name,fullPhone,countryCode,dob,"HCW",gender,hcwSkill);
            SessionProperties.getInstance(mCtx).setHcwName(name);
            SessionProperties.getInstance(mCtx).setHcwGUID(guid.toString());
            Intent i = new Intent(HCWVerifyMsg.this, HCWCamera.class);
            i.putExtras(getIntent());
            i.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
            startActivity(i);
            finish();
          //  startActivityForResult(i,IdMDefinitions.ADD_INTENT_REQ);
        }
        else {
            mVerifyUserBtn.setEnabled(true);
            verifyUserActivity();
            //startActivityForResult(i,66);
            Toast.makeText(HCWVerifyMsg.this.getApplicationContext(), "Unable to verify user due to technical issue", Toast.LENGTH_LONG).show();
        }
    }

    private void setHcwNameDobPhone() {
        mDobHcw = (TextView) findViewById(R.id.dobhcw);
        if(SessionProperties.getInstance(mCtx).getHCWDOB().length()>0) {
            mDobHcw.setText("Dob : " + SessionProperties.getInstance(mCtx).getHCWDOB());
        }
        else
            mDobHcw.setText("Dob : NA");
        mNameHcw = (TextView) findViewById(R.id.namehcw);
        if(SessionProperties.getInstance(mCtx).getHcwName().length()>0) {
            mNameHcw.setText("Name : "+ SessionProperties.getInstance(mCtx).getHcwName());
        }
        else
            mNameHcw.setText("Name: NA");
        mPhoneNoHcw = (TextView) findViewById(R.id.phoneNohcw);
        if(SessionProperties.getInstance(mCtx).getHCWPhone().length()>0) {
            mPhoneNoHcw.setText("Phone : "+SessionProperties.getInstance(mCtx).getHCWCountryCode()+SessionProperties.getInstance(mCtx).getHCWPhone());
        }
        else
            mPhoneNoHcw.setText("Phone: NA");
    }


    private void verifyUserActivity() {
        Intent i = new Intent(
                HCWVerifyMsg.this,
                HCWVerifyMsg.class);
        i.putExtras(getIntent());
        i.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
        startActivity(i);
        finish();
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        } else
        {
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    /**
     * Open the alert dialog box, set the title, message. On click "Yes" clear the session properties and exit the IDM app and if click "No" remove the dialog popup and continue with the application.
     */
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setTitle("Exiting MGD")
        .setMessage("Are you sure you want to exit this application?")
        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            /**
             * 
             * @param dialog
             * @param which
             */
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SessionProperties.getInstance(mCtx).clearAll();
                finishAndRemoveTask();
                finish();
            }

        })
        .setNegativeButton("No", null)
        .show();
    }
}
