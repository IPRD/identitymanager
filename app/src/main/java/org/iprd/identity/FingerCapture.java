package org.iprd.identity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dhi.identitymodule.SegmentationAndMatching;
import com.neurotec.biometrics.NBiometricStatus;
import com.neurotec.biometrics.NFAttributes;
import com.neurotec.biometrics.NFinger;
import com.neurotec.biometrics.NFrictionRidge;
import com.neurotec.biometrics.NSubject;
import com.neurotec.biometrics.client.NBiometricClient;
import com.neurotec.devices.NDevice;
import com.neurotec.devices.NDeviceType;
import com.neurotec.devices.NFScanner;
import com.neurotec.devices.event.NBiometricDeviceCapturePreviewEvent;
import com.neurotec.devices.event.NBiometricDeviceCapturePreviewListener;
import com.neurotec.images.NImage;
import com.neurotec.io.NFile;
import com.neurotec.util.concurrent.CompletionHandler;

import java.io.File;
import java.io.IOException;

/**
 * This class is meant to capture fingerprints from the Watson mini device.
 * In this screen, there are options to send a request for fingeprint to the device and obtain them.
 * It also displays the finger to capture.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class FingerCapture extends AppCompatActivity implements NBiometricDeviceCapturePreviewListener {
    enum State{INIT,CAPTURE,NONE};
    State mState=State.NONE;
    int mDeviceCount=0;
    String mImage="";
    private static NBiometricClient mBiometricClient = null;
    private static boolean mInitialized = false;
//  private static NeuroLicense nLic=null;
    static String TAG=FingerCapture.class.getName();
    boolean mCaptureStarted=false;
    private Context mCtx=null;
    private boolean mIntentcall=false;
    private Button mNextBtn;
    private Button mBackBtn;
    private Button mScanBtn;
    //private Button mReplicateBtn;
    private Button mClearBtn;
    private EditText mUserData;
    private TextView mUserTxt;
    private ImageView mUserFingerR, mUserFingerL,mUserFingerSmall;
    private ImageView mImageResult;
    private ImageView mUserEyes;
    private TextView mSearchResult, mPersonName;
    private ProgressBar mProgressBar, mCyclicProgressBar;
    private CheckBox mConsentCheckBox1;
    private CheckBox mConsentCheckBox2;
    private Button mPreferrenceSettingBtn;
    private TextView mYears,mMonths;
    private String mFinger;


    /**
     * for cancel the capture, taken by bio-metric client.
     */
    void CancelCapture(){
        if(mCaptureStarted) mBiometricClient.cancel();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_data);
        mImage="";
        mFinger = "";
        mCtx = getApplicationContext();
        mIntentcall = false;
        mUserData = (EditText) findViewById(R.id.userData);
        mUserTxt = (TextView) findViewById(R.id.userTxt);
        mUserFingerR = (ImageView) findViewById(R.id.rightFinger);
        mUserFingerL = (ImageView) findViewById(R.id.leftFinger);
        mUserFingerSmall = (ImageView) findViewById(R.id.fingerSmall);
        mImageResult = (ImageView) findViewById(R.id.imageResult);
        mUserEyes = (ImageView) findViewById(R.id.eyes);
        mNextBtn = (Button) findViewById(R.id.nxtBtn);
        mBackBtn = (Button) findViewById(R.id.backBtn);
        mScanBtn = (Button) findViewById(R.id.scanBtn);
        mClearBtn = (Button) findViewById(R.id.clearBtn);
        mSearchResult = (TextView) findViewById(R.id.searchResult);
        mPersonName = (TextView) findViewById(R.id.personName);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mCyclicProgressBar = (ProgressBar) findViewById(R.id.progressBar_cyclic);
        mConsentCheckBox1 = (CheckBox) findViewById(R.id.consentCheckBox);
        mConsentCheckBox2 = (CheckBox) findViewById(R.id.consentCheckBoxBiometric);
        mPreferrenceSettingBtn = (Button) findViewById(R.id.preferrenceSettingBtn);
        mYears = (TextView) findViewById(R.id.Years);
        mMonths= (TextView) findViewById(R.id.Months);

        setScreenDataForCapture();

        mScanBtn.setOnClickListener(new View.OnClickListener() {
            /**
             *  On click the scan button, run a Async task for capturing the finger print data, if not captured, enabled the capture scan button.
             * @param view : anroid View Class Object.
             */
            @Override
            public void onClick(View view) {
                disableNextAndBackButton(false);
                mUserFingerR.setImageBitmap(null);
                SetRightFingerBackGround();
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        new Thread(new Runnable() {
                            public void run() {
                                showProgressBar(true);
                                InitializeDevice();
                                CaptureFinger();
                                if(!mCaptureStarted){
                                    EnableScanButton();
                                }
                            }
                        }).start();
                    }
                });
            }
        });


        mBackBtn.setOnClickListener(new View.OnClickListener() {
            /**
             * Cancel the capturing and finish the intant call
             * @param view
             */
            @Override
            public void onClick(View view) {
                CancelCapture();
                finish();
            }
        });

        mNextBtn.setOnClickListener(new View.OnClickListener() {
            /**
             * Cancel the capturing and finish the intant call
             * @param view
             */
            @Override
            public void onClick(View view) {
                CancelCapture();
                finish();
            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mFinger = extras.getString("finger");
            String m_state = extras.getString("capture");
            if(m_state != null && m_state.contains("capture")) {
                mIntentcall = true;
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        mScanBtn.callOnClick();
                    }
                });
            }
            if(m_state != null && m_state.contains("init")) {
                mIntentcall = true;
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        InitializeDevice();
                        finish();
                    }
                });
            }
        }
        if (mFinger.equals("RIGHT"))
            mUserTxt.setText("Right Finger Scan");
        else
            mUserTxt.setText("Left Finger Scan");
    }

    private void SetRightFingerBackGround() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                    mUserFingerR.setBackgroundColor(Color.parseColor("#ececec"));
            }
        });

    }

    private void setScreenDataForSmallFinger(boolean isRightFinger) {
        mUserData.getText().clear();
        mScanBtn.setVisibility(View.INVISIBLE);
        mUserFingerSmall.setVisibility(View.VISIBLE);
        if (isRightFinger)
            mUserFingerSmall.setImageResource(R.drawable.palmindexrightsmall);
        else
            mUserFingerSmall.setImageResource(R.drawable.palmindexleftsmall);
    }

    private void EnableScanButton(){
        mCaptureStarted = false;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                disableNextAndBackButton(true);
                mScanBtn.setVisibility(View.VISIBLE);
                showProgressBar(false);
                if(mIntentcall) finish();
            }
        });
    }

    private void showProgressBar(final boolean b) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            mCyclicProgressBar.setVisibility(b?View.VISIBLE:View.INVISIBLE);
            }
        });
    }

    private void InitializeDevice() {
        mState= State.INIT;
        if (!mInitialized) {
            try {
                mBiometricClient = SegmentationAndMatching.getInstance(getApplicationContext()).getBiometricClient();
                Log.d(TAG,"Initilized the biometric client");
            } catch (ExceptionInInitializerError var1) {
                Log.d(TAG, "NeuroScanner Error");
                Log.d(TAG, var1.getMessage());
            } catch (Exception ex) {
                Log.d(TAG, ex.getMessage());
                ex.printStackTrace();
            }
        }
        getScanner();
    }

    private void SetButton(Button b1,boolean b) {
        b1.setEnabled(b);
        b1.setClickable(b);
        b1.setBackgroundResource(b?R.drawable.blue_rounded_background:R.drawable.grey_rounded_background);
    }


    private void disableNextAndBackButton(final boolean b) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SetButton(mScanBtn,b);
            }
        });
    }

    @Override
    public void finish() {
        Intent data = new Intent();
        if(mState == State.INIT) {
            data.putExtra("init", ((mDeviceCount >0)) ? "success" : "fail");
        }else  if(mState == State.CAPTURE) {
            data.putExtra("capture", (mImage.length()>0) ? "success" : "fail");
            data.putExtra("image",mImage);
        }
        mBiometricClient.cancel();
        setResult(RESULT_OK, data);
        super.finish();
    }

    /**
     *
     * @param image
     * @param userStatus
     * @param imageName
     * @param isFinal
     * @return : true
     */
    protected final boolean onImage(final NImage image, String userStatus, String imageName, boolean isFinal) {
        Log.d(TAG,userStatus);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            mUserFingerR.setVisibility(View.VISIBLE);
            mUserFingerR.setImageBitmap(image.toBitmap());
            }
        });
        return true;
    }


    private CompletionHandler<NBiometricStatus, NSubject> completionHandler = new CompletionHandler<NBiometricStatus, NSubject>() {
        /**
         * Finger data will save on mentioned path andenable the scan button.
         * @param result
         * @param subject
         */
        @Override
        public void completed(NBiometricStatus result, NSubject subject) {
            if (result == NBiometricStatus.OK) {
                String basedir= Environment.getExternalStorageDirectory().getPath();
                String licpath= basedir + "/Neurotechnology/Licenses";
                File outputFile = new File(licpath, "finger-from-scanner.bmp");
                try {
                    subject.getFingers().get(0).getImage().save(outputFile.getAbsolutePath());
                    mImage = outputFile.getAbsolutePath();
                    Log.d(TAG,"Size of image "+ Integer.toString(mImage.length()));
                    Log.d(TAG,"Saved finger to "+ outputFile.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                outputFile = new File(licpath, "finger-template-from-scanner.dat");
                try {
                    NFile.writeAllBytes(outputFile.getAbsolutePath(), subject.getTemplateBuffer());
                    Log.d(TAG,"Saved finger template to "+ outputFile.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                 Log.d(TAG,"Status="+result.toString());
            }
            EnableScanButton();
        }

        /**
         * if failed() is called, enable the scan button.
         * @param exc
         * @param subject
         */
        @Override
        public void failed(Throwable exc, NSubject subject) {
            EnableScanButton();
            exc.printStackTrace();
        }
    };

    /**
     * This is for displaying the capture data whic is given by bio-metric device preview events.
     * @param event
     */
    @Override
    public void capturePreview(NBiometricDeviceCapturePreviewEvent event) {
        boolean force = onImage((NFrictionRidge)event.getBiometric(), false);
    }


    private boolean onImage(NFrictionRidge biometric, boolean isFinal) {
        StringBuilder sb = new StringBuilder();
        sb.append(biometric.getStatus());
        for (NFAttributes obj : biometric.getObjects()) {
            sb.append("\n");
            sb.append(String.format("\t{%s}: {%s}", obj.getPosition(), obj.getStatus()));
        }
        return onImage(biometric.getImage(), sb.toString(), (biometric.getStatus() != NBiometricStatus.NONE ? biometric.getStatus() : NBiometricStatus.OK).toString(), isFinal);
    }

    private NFScanner getScanner() {
        Log.d(TAG,"Get Scanner info");
        NDevice fingerDevice = null;
        for (NDevice device : mBiometricClient.getDeviceManager().getDevices()) {
			Log.i("Device", String.format("Device name => %s", device.getDisplayName()));
            if (device.getDeviceType().contains(NDeviceType.FSCANNER)) {
                fingerDevice = device;
                mDeviceCount=1;
            }
        }
        return (NFScanner) fingerDevice;
    }

    /**
     * this method is used for capturing the finger print.
     */
    public void CaptureFinger(){
        mState = State.CAPTURE;
        if(mCaptureStarted) return;
        mImage="";
        try {
            NSubject subject = new NSubject();
            NFinger finger = new NFinger();
            NFScanner fScanner = getScanner();
            if(fScanner != null){
               Log.d(TAG,"Found fingerprint scanner:;"+ fScanner.getModel() + fScanner.getDisplayName());
            } else {
                return;
            }
            mUserFingerR.setImageBitmap(null);
            if (mFinger.equals("RIGHT"))
                setScreenDataForSmallFinger(true);
            else
                setScreenDataForSmallFinger(false);
            fScanner.addCapturePreviewListener(this);
            subject.getFingers().add(finger);
            mBiometricClient.createTemplate(subject, subject, completionHandler);
            mCaptureStarted = true;
        } catch (Exception ex) {
            Log.e(TAG,ex.getMessage());
        }
    }

    private void setScreenDataForCapture() {
        mUserData.getText().clear();
        if (mFinger.equals("RIGHT"))
            mUserTxt.setText("Right Finger Scan");
        else
            mUserTxt.setText("Left Finger Scan");
        mYears.setVisibility(View.INVISIBLE);
        mMonths.setVisibility(View.INVISIBLE);
        mPreferrenceSettingBtn.setVisibility(View.INVISIBLE);
        mClearBtn.setVisibility(View.INVISIBLE);
        mUserEyes.setVisibility(View.INVISIBLE);
        mUserData.setVisibility(View.INVISIBLE);
        mUserFingerR.setVisibility(View.INVISIBLE);
        mUserFingerL.setVisibility(View.INVISIBLE);
        mNextBtn.setVisibility(View.INVISIBLE);
        mImageResult.setVisibility(View.INVISIBLE);
        mConsentCheckBox1.setVisibility(View.INVISIBLE);
        mConsentCheckBox2.setVisibility(View.INVISIBLE);

        mBackBtn.setVisibility(View.VISIBLE);
        mUserFingerR.setImageBitmap(null);
        mUserFingerR.setVisibility(View.VISIBLE);
        mClearBtn.setVisibility(View.VISIBLE);
        mUserTxt.setVisibility(View.VISIBLE);
        mScanBtn.setText("Scan Finger");
        mScanBtn.setVisibility(View.VISIBLE);
        mNextBtn.setVisibility(View.VISIBLE);
    }
}
