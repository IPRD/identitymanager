package org.iprd.identity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.dhi.identitymodule.IdentityManager;
import com.dhi.identitymodule.ImageUtils;
import com.dhi.identitymodule.SessionProperties;

import java.io.File;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.Manifest.permission_group.CAMERA;

/**
 * This class will launch the android camera application for patients.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */

public class PatientCamera extends AppCompatActivity {
    private Camera camera;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private static final int TAKE_PICTURE = 1;
    Button mNxtButton;
    Bitmap mCamImage = null;
    private Context mCtx;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCtx = getApplicationContext();
        if(!checkpermission()){
            requestPermission();
        }

        setContentView(R.layout.activity_patient_camera);
        mNxtButton = (Button) findViewById(R.id.patientCamNextBtn);
        mNxtButton.setBackground(getResources().getDrawable(R.drawable.grey_rounded_background,null));
        mNxtButton.setEnabled(false);

        final ImageButton bckButton = (ImageButton) findViewById(R.id.patientCamBackBtn);
        bckButton.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                bckButton.setEnabled(false);
                finish();
            }
        });

        cameraCaptureOnClicked();
        nextButtonOnCLicked();

        ImageButton login = (ImageButton) findViewById(R.id.hcwLgnPatientCam);
        login.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                Intent i = new Intent(PatientCamera.this, MainActivity.class);
                SessionProperties.getInstance(mCtx).clearAll();
                finish();
                startActivity(i);
            }
        });

        ImageButton exitout = (ImageButton) findViewById(R.id.exitPatientCam);
        exitout.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ImageButton plogin = (ImageButton) findViewById(R.id.patientLgnPatientCam);
        plogin.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
          /*      Intent i = new Intent(PatientCamera.this,PatientEnroll.class);
                i.putExtras(getIntent());
                SessionProperties.getInstance(mCtx).clearPatient();
                finish();
                startActivity(i); */
            }
        });
    }

    private void nextButtonOnCLicked() {
        mNxtButton.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                mNxtButton.setEnabled(false);
                StringBuffer guid  = new StringBuffer();
                StringBuffer missingParams = new StringBuffer();
                IdentityManager.getIDMInstance().AddPerson(IdmSessionProperties.getSessionInstance(mCtx).getSessionCtx(), SessionProperties.getInstance(mCtx).getPatientGUID(),mCtx,"","","",0,"","","","","","","Patient", mCamImage,"","",IdmSessionProperties.getSessionInstance(mCtx).getmPatientBioJSON(),guid,missingParams);

                setResult(RESULT_OK);
                finish();
            }
        });
    }


    private void cameraCaptureOnClicked() {
        final File photo = new File(Environment.getExternalStorageDirectory(),  getResources().getString(R.string.patientPicName));
        photo.delete();

        Button cameraCapture = (Button) findViewById(R.id.backCaptureBtn);

        final ImageView image = (ImageView) findViewById(R.id.imagePatient);
        image.setImageResource(R.drawable.empty_profile);
        cameraCapture.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param v
             */
            @Override
            public void onClick(View v) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            Uri photoUri = FileProvider.getUriForFile(PatientCamera.this,
                    BuildConfig.APPLICATION_ID + ".provider",
                    photo);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            startActivityForResult(intent, TAKE_PICTURE);
            }
        });
    }

    /**
     * onBackPressed() :  pop-up dialog for exit the MGD app or cancel the pop-up dialog box.
     */
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setTitle("Exiting MGD")
        .setMessage("Are you sure you want to exit this application?")
        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            /**
             *
             * @param dialog
             * @param which
             */
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SessionProperties.getInstance(mCtx).clearAll();
                finishAndRemoveTask();
                finish();
            }

        })
        .setNegativeButton("No", null)
        .show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        final ImageView image = (ImageView) findViewById(R.id.imagePatient);

        if (requestCode == TAKE_PICTURE && resultCode == Activity.RESULT_OK) {
            String path = Environment.getExternalStorageDirectory()+"/"+getResources().getString(R.string.patientPicName);
            mCamImage = ImageUtils.readImageAndAlignExif(path);
            BioUtils.SaveJpegImage(mCamImage,path);
            mCamImage = BioUtils.resizeBitmap(path,720,1280);
            BioUtils.SaveJpegImage(mCamImage,path);

            image.setImageBitmap(mCamImage);
            mNxtButton.setBackground(getResources().getDrawable(R.drawable.blue_rounded_background,null));
            mNxtButton.setEnabled(true);
        } else if (resultCode == RESULT_OK && requestCode == IdMDefinitions.ADD_INTENT_REQ) {
            setResult(RESULT_OK);
            finish();
        } else {
            setResult(RESULT_CANCELED);
            finish();
        }

    }

    private boolean checkpermission(){
        int res = ContextCompat.checkSelfPermission(mCtx, CAMERA);
        int res1 = ContextCompat.checkSelfPermission(PatientCamera.this, WRITE_EXTERNAL_STORAGE);
        return res1 == PackageManager.PERMISSION_GRANTED && res == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission(){
        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE, CAMERA}, PERMISSION_REQUEST_CODE);
    }
}