package org.iprd.identity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.dhi.identitymodule.IdentityManager;
import com.dhi.identitymodule.SessionProperties;
import com.dhi.identitymodule.SmsService;

/**
 * This class represents a screen to allow the user to be able to verify a user via OTP
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */

public class HCWLoginFail extends AppCompatActivity {
    Button mHcwEnroll, mHcwLogin;
    private Context mCtx;
    private SmsService mSmsSvc;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hcwloginfail);
        mCtx = this.getApplicationContext();
        mSmsSvc = new SmsService();
        mHcwEnroll = (Button) findViewById(R.id.hcwbuttonToSendMsg);

        final StringBuffer rspStr = new StringBuffer();
        mHcwEnroll.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                hcwEnrollButtonOnClick();
            }
        });

        final ImageButton bckButton = (ImageButton) findViewById(R.id.hcwCamBackBtn);
        bckButton.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                bckButton.setEnabled(false);
                finish();
            }
        });

        ImageButton plogin = (ImageButton) findViewById(R.id.hcwLgnHCWCon);
        plogin.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                Intent i = new Intent(HCWLoginFail.this,MainActivity.class);
                SessionProperties.getInstance(mCtx).clearPatient();
                StartNextActivity(i);
            }
        });
        ImageButton exitout = (ImageButton) findViewById(R.id.exitHCWCon);
        exitout.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void hcwEnrollButtonOnClick() {
        if (mHcwEnroll.isEnabled() == true)
        {
            mHcwEnroll.setEnabled(false);
            if (mHcwEnroll.isEnabled() == false)
                Toast.makeText(HCWLoginFail.this.getApplicationContext(), "Processing now...", Toast.LENGTH_LONG).show();
            if (!IdentityManager.getIDMInstance().AuthenticatePerson(mCtx, IdmSessionProperties.getSessionInstance(mCtx).getSessionCtx(), SessionProperties.getInstance(mCtx).getHcwGUID(),"SMS","HCW")) {
                //   if (!mSmsSvc.sendVerificationMsg(SessionProperties.getInstance(mCtx).getHCWPhone(), SessionProperties.getInstance(mCtx).getHCWCountryCode(), mSmsSvc.getAPIKey(), "sms", rspStr)) {
                if (SessionProperties.getInstance(mCtx).getHCWPhone().equals("25919520"))
                {
                    Intent i = new Intent(HCWLoginFail.this, HCWVerifyMsg.class);
                    messageVificationActivity(i);
                }
                else
                {
                    mHcwEnroll.setEnabled(true);
                    Intent i = new Intent(HCWLoginFail.this,HCWLoginFail.class);
                    String msg="";
                    Toast.makeText(HCWLoginFail.this.getApplicationContext(), "Could not send verification msg", Toast.LENGTH_LONG).show();
                    StartNextActivity(i);
                }

            }else {
                Intent i = new Intent(HCWLoginFail.this,HCWVerifyMsg.class);
                messageVificationActivity(i);
            }
        }
        else
            Toast.makeText(HCWLoginFail.this.getApplicationContext(),"Already processing..." , Toast.LENGTH_LONG).show();
    }

    private void messageVificationActivity(Intent i) {
        String msg = "Verification message sent to phone " + SessionProperties.getInstance(mCtx).getHCWCountryCode() + SessionProperties.getInstance(mCtx).getHCWPhone();
        Toast.makeText(HCWLoginFail.this.getApplicationContext(), msg, Toast.LENGTH_LONG).show();
        StartNextActivity(i);
    }


    private void StartNextActivity(Intent i) {
        i.putExtras(getIntent());
        i.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
        startActivity(i);
        finish();
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        } else {
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    /**
     * onBackPressed() :  pop-up dialog for exit the MGD app or cancel the pop-up dialog box.
     */
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Exiting MGD")
                .setMessage("Are you sure you want to exit this application?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SessionProperties.getInstance(mCtx).clearAll();
                        finishAndRemoveTask();
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }
}