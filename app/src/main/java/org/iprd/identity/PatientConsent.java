package org.iprd.identity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;

import com.dhi.identitymodule.IdentityManager;
import com.dhi.identitymodule.SessionProperties;
/**
 *  * This class is used to display the consent screen for patients.
 *  * At this point, this functionality is integrated in UserInput.java.
 *  * However, we have retained this screen in the project
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class PatientConsent extends AppCompatActivity {
    CheckBox mPatientConsentCheckBox1, mPatientConsentCheckBox2;
    Button mNxtButton;
    private Context mCtx;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_consent);
        mCtx = this.getApplicationContext();

        final ImageButton bckButton = (ImageButton) findViewById(R.id.patientConBackBtn);
        bckButton.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                bckButton.setEnabled(false);
                Intent i = new Intent(PatientConsent.this,PatientCamera.class);
                i.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                startActivity(i);
                finish();
            }
        });


        mNxtButton = (Button) findViewById(R.id.nxtPatientConsent);
        mNxtButton.setBackground(getResources().getDrawable(R.drawable.grey_rounded_background,null));
        mNxtButton.setEnabled(false);

        mPatientConsentCheckBox1 = (CheckBox) findViewById(R.id.patientCsntChkBox1);
        mPatientConsentCheckBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            /**
             *
             * @param buttonView
             * @param isChecked
             */
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked ) {
                    if(mPatientConsentCheckBox2.isChecked()) {
                        mNxtButton.setBackground(getResources().getDrawable(R.drawable.blue_rounded_background, null));
                        mNxtButton.setEnabled(true);
                    }

                }else{
                    mNxtButton.setBackground(getResources().getDrawable(R.drawable.grey_rounded_background,null));
                    mNxtButton.setEnabled(false);
                }

            }
        });
        mPatientConsentCheckBox2 = (CheckBox) findViewById(R.id.patientCsntChkBox2);
        mPatientConsentCheckBox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            /**
             *
             * @param buttonView
             * @param isChecked
             */
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked ) {
                    if(mPatientConsentCheckBox1.isChecked()) {
                        mNxtButton.setBackground(getResources().getDrawable(R.drawable.blue_rounded_background,null));
                        mNxtButton.setEnabled(true);
                    }
                }else{
                    mNxtButton.setBackground(getResources().getDrawable(R.drawable.grey_rounded_background,null));
                    mNxtButton.setEnabled(false);
                }

            }
        });
        mNxtButton.setOnClickListener(new View.OnClickListener() {
            /**
             * here, add this patient consent transaction to backend DB and
             * @param view
             */
            @Override
            public void onClick(View view) {
                //these are hardcoded values. pls make sure we populate these with actual values.
                mNxtButton.setEnabled(false);
                String content = "Agreed to Consent";
                StringBuffer guid = new StringBuffer();
                StringBuffer missingParamsStringBuffer = new StringBuffer();
                IdentityManager.getIDMInstance().AddPerson(IdmSessionProperties.getSessionInstance(mCtx).getSessionCtx(), SessionProperties.getInstance(mCtx).getPatientGUID(),mCtx,"","","",0,"","","","","","","Patient",null,content,"",IdmSessionProperties.getSessionInstance(mCtx).getmPatientBioJSON(),guid,missingParamsStringBuffer);
                Log.d("PatConsent screen",""+SessionProperties.getInstance(mCtx).getHCWPhotoStr());
                setResult(RESULT_OK);
                finish();
            }
        });
        ImageButton login = (ImageButton) findViewById(R.id.hcwLgnPatientCon);
        login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent i = new Intent(PatientConsent.this, MainActivity.class);
                SessionProperties.getInstance(mCtx).clearAll();
                finish();
                startActivity(i);
            }
        });
        ImageButton exitout = (ImageButton) findViewById(R.id.exitPatientCon);
        exitout.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        ImageButton plogin = (ImageButton) findViewById(R.id.patientLgnPatientCon);
        plogin.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
         /*       Intent i = new Intent(PatientConsent.this,PatientEnroll.class);
                i.putExtras(getIntent());
                SessionProperties.getInstance(mCtx).clearPatient();
                finish();
                startActivity(i); */
            }
        });
    }

    /**
     * onBackPressed() :  pop-up dialog for exit the MGD app or cancel the pop-up dialog box.
     */
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setTitle("Exiting MGD")
        .setMessage("Are you sure you want to exit this application?")
        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            /**
             *
             * @param dialog
             * @param which
             */
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SessionProperties.getInstance(mCtx).clearAll();
                finishAndRemoveTask();
                finish();
            }
        })
        .setNegativeButton("No", null)
        .show();
    }

    /**
     *
     */
    public void finish() {
        super.finish();
    }
}