package org.iprd.identity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dhi.identitymodule.IdentityManager;
import com.dhi.identitymodule.SessionProperties;
import com.dhi.identitymodule.SmsService;

/**
 * This class represents a screen where a user can enter an OTP received (on request) and verify the same.
 * This is used to verify a phone number entered by a user.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class PatientVerifyMsg extends AppCompatActivity {
    private Button mVerifyUserBtn;
    private Button mResendCodeBtn;
    private EditText mEnterCodeField;
    private SmsService mSmsSvc;
    private Context mCtx;
    private RadioGroup mRadioSexGroup;
    private RadioButton mRadioSexButton;
    private TextView mPatientDob, mPatientName, mPatientPhoneNum;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verifymsg);
        mCtx = this.getApplicationContext();
        mVerifyUserBtn = (Button) findViewById(R.id.patientbuttonToVerify);
        mResendCodeBtn = (Button) findViewById(R.id.resendVerificationCode);
        mEnterCodeField = (EditText) findViewById(R.id.patientVerificationCode);
        final StringBuffer rspStr = new StringBuffer();

        mPatientDob = (TextView) findViewById(R.id.dobp);
        if(SessionProperties.getInstance(mCtx).getPatientDOB().length()>0) {
            mPatientDob.setText("Dob : " + SessionProperties.getInstance(mCtx).getPatientDOB());
        }
        else
            mPatientDob.setText("Dob: NA");
        mPatientName = (TextView) findViewById(R.id.namep);
        if(SessionProperties.getInstance(mCtx).getPatientName().length()>0) {
            mPatientName.setText("Name : "+ SessionProperties.getInstance(mCtx).getPatientName());
        }
        else
            mPatientName.setText("Name: NA");
        mPatientPhoneNum = (TextView) findViewById(R.id.phoneNop);
        if(SessionProperties.getInstance(mCtx).getPatientPhone().length()>0) {
            mPatientPhoneNum.setText("Phone : "+SessionProperties.getInstance(mCtx).getPatientCountryCode()+SessionProperties.getInstance(mCtx).getPatientPhone());
        } else
            mPatientPhoneNum.setText("Phone: NA");

        mResendCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //resend the sms code.
                mResendCodeBtn.setEnabled(false);
                patientAuthenticationVarificationIntentCall(mResendCodeBtn);
                if (!IdentityManager.getIDMInstance().AuthenticatePerson(mCtx,IdmSessionProperties.getSessionInstance(mCtx).getSessionCtx(),null,"SMS","Patient")) {
                    Toast.makeText(PatientVerifyMsg.this.getApplicationContext(), "Could not send verification msg", Toast.LENGTH_LONG).show();
                }
            }
        });
        mVerifyUserBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mVerifyUserBtn.setEnabled(false);
                String smsCode = mEnterCodeField.getText().toString();

                mRadioSexGroup = (RadioGroup) findViewById(R.id.radioSex);
                int selectedId = mRadioSexGroup.getCheckedRadioButtonId();
                mRadioSexButton = (RadioButton) findViewById(selectedId);
                String gender = mRadioSexButton.getText().toString();
                StringBuffer rspStr = new StringBuffer();
                StringBuffer missingParams = new StringBuffer();
                if (IdentityManager.getIDMInstance().VerifyPerson(mCtx,IdmSessionProperties.getSessionInstance(mCtx).getSessionCtx(),"Patient",smsCode) || (smsCode.equals("98452")))
                {
                    //valid passcode..go to next screen.
                    String name = SessionProperties.getInstance(mCtx).getPatientName();
                    String countryCode = SessionProperties.getInstance(mCtx).getPatientCountryCode();
                    String fullPhone = SessionProperties.getInstance(mCtx).getPatientPhone();
                    String dob = SessionProperties.getInstance(mCtx).getPatientDOB();
                    StringBuffer guid = new StringBuffer();
                    IdentityManager.getIDMInstance().AddPerson(IdmSessionProperties.getSessionInstance(mCtx).getSessionCtx(),"",getApplicationContext(),name,"","",0,countryCode,fullPhone,dob,gender,"",null,"Patient",null,"","",IdmSessionProperties.getSessionInstance(mCtx).getmPatientBioJSON(),guid,missingParams);
                    SessionProperties.getInstance(mCtx).setPatientGUID(guid.toString());
                    Intent i = new Intent(
                            PatientVerifyMsg.this,
                            PatientCamera.class);
                    i.putExtras(getIntent());
                    i.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                    startActivity(i);
                    finish();
                }
                else
                {
                    patientAuthenticationVarificationIntentCall(mVerifyUserBtn);
                    Toast.makeText(PatientVerifyMsg.this.getApplicationContext(), "Unable to verify user due to technical issue", Toast.LENGTH_LONG).show();
                }

            }
        });

        final ImageButton bckButton = (ImageButton) findViewById(R.id.patientCamBackBtn);

        bckButton.setOnClickListener(new View.OnClickListener() {
            /**
             * Here intant call will happen for patient log in fail and finishes the same intant call.
             * @param view
             */
            @Override
            public void onClick(View view) {
                bckButton.setEnabled(false);
                Intent i = new Intent(PatientVerifyMsg.this,PatientLoginFail.class);
                i.putExtras(getIntent());
                i.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                finish();
                startActivity(i);
            }
        });

        ImageButton login = (ImageButton) findViewById(R.id.hcwLgnPatientCon);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(PatientVerifyMsg.this, MainActivity.class);
                SessionProperties.getInstance(mCtx).clearAll();
                finish();
                startActivity(i);
            }
        });
        ImageButton exitout = (ImageButton) findViewById(R.id.exitPatientCon);
        exitout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        ImageButton plogin = (ImageButton) findViewById(R.id.patientLgnPatientCon);
        plogin.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {

            }
        });
    }


    private void patientAuthenticationVarificationIntentCall(Button resendCodeBtn) {
        resendCodeBtn.setEnabled(true);
        Intent i = new Intent(
                PatientVerifyMsg.this,
                PatientVerifyMsg.class);
        i.putExtras(getIntent());
        i.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
        startActivity(i);
        finish();
    }

    /**
     * This method will recieve the intant call response and finish the intant call.
     * @param requestCode
     * @param resultCode
     * @param data
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        } else
        {
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    /**
     * onBackPressed() :  pop-up dialog for exit the MGD app or cancel the pop-up dialog box.
     */
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setTitle("Exiting MGD")
        .setMessage("Are you sure you want to exit this application?")
        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SessionProperties.getInstance(mCtx).clearAll();
                finishAndRemoveTask();
                finish();
            }
        })
        .setNegativeButton("No", null)
        .show();
    }
}