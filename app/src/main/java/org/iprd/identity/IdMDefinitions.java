package org.iprd.identity;
/**
 * values for various Request messages sent via intent call or API's
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class IdMDefinitions {
    public static int CONNECT = 1;
    public static int ADD_PERSON = 2;
    final int HANDLE_POS = 3;
    public static int NO_REQ = 0;
    public static int CONNECT_INTENT_REQ = 10;
    public static int ADD_INTENT_REQ = 11;
    public static int POS_INTENT_REQ = 12;
    public static int PSIG_INTENT_REQ = 13;
    public static int POS_SCREEN_INTENT_REQ = 14;
}
