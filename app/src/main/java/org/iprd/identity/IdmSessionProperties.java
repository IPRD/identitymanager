package org.iprd.identity;

import android.content.Context;
import android.content.SharedPreferences;

import com.dhi.identitymodule.AppType;
import com.dhi.identitymodule.BuildType;


/**
 * This class is used for maintaining the session properties within the context of identity manager.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class IdmSessionProperties {
    private String mSessionCtx;
    private String mHcwGUID;
    private String mPatientGUID;
    private String mUserType;
    private String mUserName;
    private String mUserDOB;
    private String mUserID;
    private String mHcwBioJSON;
    private String mPatientBioJSON;
    private com.dhi.identitymodule.AppType mAppFlag; //this is a flag to indicate a specific app type. ex: general or biometric
    private BuildType mBuildFlag; //dev or release
    private static IdmSessionProperties mInstance = null;
    private SharedPreferences mPref;
    private SharedPreferences.Editor mEdit;

    private IdmSessionProperties(String sessionCtx,String hcwGUID,String patientGUID) {
        mSessionCtx = sessionCtx;
        mHcwGUID = hcwGUID;
        mPatientGUID = patientGUID;
    }

    private IdmSessionProperties(Context ctx) {
        mPref = ctx.getSharedPreferences("IdMSession", Context.MODE_PRIVATE);
        mEdit = mPref.edit();
    }
    private IdmSessionProperties(){

    }

    public String getSessionCtx() {
        String exValue = "";
        try {
            return mPref.getString("SessionContext", exValue);
        } catch (ClassCastException e) {
            return exValue;
        }
    }

    public void setSessionCtx(String sessionCtx) {
        mEdit.putString("SessionContext",sessionCtx);
        mEdit.commit();
    }

    public String getUserType() {
        return mUserType;
    }

    public void setUserType(String userType) {
        this.mUserType = userType;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        this.mUserName = userName;
    }

    public String getUserDOB() {
        return mUserDOB;
    }

    public void setUserDOB(String userDOB) {
        this.mUserDOB = userDOB;
    }

    public String getUserID() {
        return mUserID;
    }

    public void setUserID(String userID) {
        this.mUserID = userID;
    }

    public String getmHcwBioJSON() {
        return mHcwBioJSON;
    }

    public void setmHcwBioJSON(String mHcwBioJSON) {
        this.mHcwBioJSON = mHcwBioJSON;
    }

    public String getmPatientBioJSON() {
        return mPatientBioJSON;
    }

    public void setmPatientBioJSON(String mPatientBioJSON) {
        this.mPatientBioJSON = mPatientBioJSON;
    }

    public com.dhi.identitymodule.AppType getAppFlag() {
        return mAppFlag;
    }

    public void setAppFlag(com.dhi.identitymodule.AppType appFlag) {
        this.mAppFlag = appFlag;
    }

    public BuildType getBuildFlag() {
        return mBuildFlag;
    }

    public void setBuildFlag(BuildType buildFlag) {
        this.mBuildFlag = buildFlag;
    }


    public static synchronized IdmSessionProperties getSessionInstance(Context ctx) {
        if (mInstance == null)
            mInstance = new IdmSessionProperties(ctx);
        return mInstance;
    }
}

