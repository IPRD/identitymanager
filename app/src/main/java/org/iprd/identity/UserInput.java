package org.iprd.identity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dhi.identitymodule.IdentityModule;
import com.dhi.identitymodule.SegmentationAndMatching;
import com.dhi.identitymodule.SessionProperties;
import com.dhi.identitymodule.AppType;
import com.dhi.identitymodule.BuildType;
import com.dhi.identitymodule.ImageUtils;


import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.iprd.identity.BioUtils.CreateImageMessage;
import static org.iprd.identity.BioUtils.getImageStringFromJson;
import static org.iprd.identity.UserDataType.USER_EYE;
import static org.iprd.identity.UserDataType.USER_FACE;
import static org.iprd.identity.UserDataType.USER_FINGER_L;
import static org.iprd.identity.UserDataType.USER_FINGER_R;
import static org.iprd.identity.UserDataType.USER_RESULT;

/**
 * This class is used to present a user wizard that can allow the user to input name, age, ID, biometric data.
 * After that, it uses a custom truth table logic to see if a user is present and if so, gets the user details and presents on screen.
 * This screen can have various states - depending on what we need to input. Ex: user consent, user name, age, etc.
 * Depending on state, the buttons on the screen are added/removed. But all these screens are just one activity - we just keep changing the buttons
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */

public class UserInput extends AppCompatActivity {
    private Button mNextBtn;
    private Spinner mAgeYears;
    private Spinner mAgeMonths;
    private ConstraintLayout mAgeLinearYear;
    private ConstraintLayout mAgeLinearMonth;
    private Button mBackBtn;
    private Button mScanBtn;
    private ImageButton mReplicateBtn;
    private Button mClearBtn;
    private Button mPreferrenceSettingBtn;
    private EditText mUserData;
    private TextView mUserTxt;
    private ImageView mUserFingerR,mUserFingerL,mUserFace;
    private ImageView mImageResult;
    private ImageView mUserEyes;
    private TextView mYears,mMonths;
    private TextView mSearchResult,mPersonName;
    private ProgressBar mProgressBar,mCyclicProgressBar;
    private CheckBox mConsentCheckBoxDeclaration1;
    private CheckBox mConsentCheckBoxDeclaration2;
    private Boolean mIsConsentCheckBox1 = false;
    private Boolean mIsConsentCheckBox2 = false;
    private String mFilenameforFingerCapture="";
    UserDataType mCurrentState;
    private BiometricManager mBiometricClient =null;
    Context mCtx;
    String name = "";
    String uid = "";
    String gid = "";
    int age = 0;
    private boolean isHCW = false;
    private boolean mIsAddUser = false;
    int CAPTURE_FINGER=7654;
    int PREFERENCES=7655;
    static boolean mIsEnabledWatsonminiPreference = false;

    private static final String INIT_REQ = "Init";
    private static final String GET_DATA = "GetData";
    private static final String OPEN_FINGER_PRINT_REQUEST = "OpenFP";
    private static final String CLOSE_FINGER_PRINT_REQUEST = "CloseFP";
    private static final String SCANE_RIGHT_FINGER_PRINT_REQUEST = "ScanFPR";
    private static final String SCANE_LEFT_FINGER_PRINT_REQUEST = "ScanFPL";
    private static final String SCANE_IRIS_REQUEST = "ScanIris";
    private static final String GET_STATUS_REQUEST = "GetStatus";
    private static final String GET_IRIS_REQUEST = "GetDataE";
    private static final String GET_LEFT_FINGER_DATA = "GetDataFL";
    private static final String GET_RIGHT_FINGER_DATA = "GetDataFR";
    private static final String CLOSE_IRIS_REQUEST = "CloseIris";
    private static final int TAKE_PICTURE = 1;

    private static SpeakOut TTS=null;
    String mBiometricInformationJson = "";
    String mLeftIndex="";
    String mRightIndex="";
    String mLeftEye="";
    String mRightEye="";
    String mFace = "";
    private boolean mIsNoImpDataProvided = false;
    private boolean mIsMultipleResFound = false;
    private boolean mIsFaceGiven = false;

    File mPhoto;

    private void CaptureFingersFromWatsonMini(String param){
        Intent intent = new Intent(UserInput.this,FingerCapture.class);
        intent.putExtra("capture",param);
        if (mCurrentState == UserDataType.USER_FINGER_R)
            intent.putExtra("finger","RIGHT");
        else if (mCurrentState == UserDataType.USER_FINGER_L)
            intent.putExtra("finger","LEFT");
        mFilenameforFingerCapture="";
        startActivityForResult(intent,CAPTURE_FINGER);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_data);
        mCtx = getApplicationContext();
        name = "";
        uid = IdmSessionProperties.getSessionInstance(mCtx).getUserID();
        gid = "";
        age = 0;

        mLeftIndex="";
        mRightIndex="";
        mLeftEye="";
        mRightEye="";
        mUserData = (EditText) findViewById(R.id.userData);
        mUserTxt = (TextView) findViewById(R.id.userTxt);
        mUserFingerR = (ImageView) findViewById(R.id.rightFinger);
        mUserFingerL = (ImageView) findViewById(R.id.leftFinger);
        mUserFace = (ImageView) findViewById(R.id.face);
        mImageResult = (ImageView) findViewById(R.id.imageResult);
        mUserEyes = (ImageView) findViewById(R.id.eyes);
        mNextBtn = (Button) findViewById(R.id.nxtBtn);
        mBackBtn = (Button) findViewById(R.id.backBtn);
        mScanBtn = (Button) findViewById(R.id.scanBtn);
        mClearBtn = (Button) findViewById(R.id.clearBtn);
        mReplicateBtn = (ImageButton) findViewById(R.id.replicateBtn);
        //if (IdmSessionProperties.getSessionInstance(mCtx).getBuildFlag() == BuildType.DEV)
            mReplicateBtn.setVisibility(View.VISIBLE);
        mSearchResult = (TextView) findViewById(R.id.searchResult);
        mPersonName = (TextView) findViewById(R.id.personName);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mCyclicProgressBar = (ProgressBar) findViewById(R.id.progressBar_cyclic);
        mConsentCheckBoxDeclaration1 = (CheckBox) findViewById(R.id.consentCheckBox);
        mConsentCheckBoxDeclaration2 = (CheckBox) findViewById(R.id.consentCheckBoxBiometric);
        mPreferrenceSettingBtn = (Button) findViewById(R.id.preferrenceSettingBtn);
        mAgeYears = (Spinner) findViewById(R.id.ageYears);
        mAgeMonths = (Spinner) findViewById(R.id.ageMonths);
        mAgeLinearYear = (ConstraintLayout) findViewById(R.id.ageLinearYear);
        mAgeLinearMonth = (ConstraintLayout) findViewById(R.id.ageLinearMonth);
        mYears = (TextView) findViewById(R.id.Years);
        mMonths = (TextView) findViewById(R.id.Months);
        final SharedPreferences sharedPreferences = getSharedPreferences();
        mPhoto =  new File(Environment.getExternalStorageDirectory(),  getResources().getString(R.string.HCWPicName));

        //enable replication button based on setting

        //verify license if required
        if (!SegmentationAndMatching.getInstance(mCtx).isLicenseCheckDone())
            SegmentationAndMatching.getInstance(mCtx).checkLicense();

        deleteHcwOrPatientOnPreferencesSelection(sharedPreferences);
        userAgeDropDownSelectionInitialization(500);

        Bundle getIntentGetExtraData = getIntent().getExtras();
        if (getIntentGetExtraData != null) {
            mCurrentState = (UserDataType) getIntentGetExtraData.getSerializable(getResources().getString(R.string.placehoder_enum));
        }
        idmLandingPageSelection();

        if(TTS == null) TTS = new SpeakOut(mCtx);
        if(!mIsEnabledWatsonminiPreference) {
            mBiometricClient = new BiometricManager(mCtx);
            mBiometricClient.createClientThread();
        }

        mNextBtn.setOnClickListener(new View.OnClickListener() {
            /**enable, clickable and setting back ground
             * On next button click, setting the button property like enabling the button true/false and setting the background color and also setting the clickable property.
             * Also we are setting the screen to be display after click event.
             * @param view : Responsible for drawing and event handling.The View class is a superclass for all GUI components.
             */
            @Override
            public void onClick(View view) {
                setButton(mNextBtn,true);
                setAndGetScreenOnNextButtonClicked();
            }
        });

        mConsentCheckBoxDeclaration1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            /**
             * This method will decide whether the active next button will display or not. if both the consent check box is checked then the active next button will be activated.
             * @param buttonView : CompoundButton class object
             * @param isChecked : Boolean type variable ,decide whether the  current checkbox( first checkbox) is checked or unchecked.
             */
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mIsConsentCheckBox1 = isChecked;
                if(mIsConsentCheckBox1 && mIsConsentCheckBox2){
                    setButton(mNextBtn,true);
                    mNextBtn.setVisibility(View.VISIBLE);
                }else{
                    setButton(mNextBtn,false);
                }
            }
        });
        mConsentCheckBoxDeclaration2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            /**
             * This method will decide whether the active next button will display or not. if both the consent check box is checked then the active next button will be activated.
             * @param buttonView : CompoundButton class object
             * @param isChecked : Boolean type variable ,decide whether the  current checkbox( secound checkbox) is checked or unchecked.
             */
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mIsConsentCheckBox2 = isChecked;
                if(mIsConsentCheckBox1 && mIsConsentCheckBox2){
                    setButton(mNextBtn,true);
                    mNextBtn.setVisibility(View.VISIBLE);
                }else{
                    setButton(mNextBtn,false);
                }
            }
        });

        mReplicateBtn.setOnClickListener(new View.OnClickListener() {
            /**
             * on click of replicate button, IdentityModule's instance(on application context) will replicate.
             * @param v : View class. Responsible for drawing and event handling.The View class is a superclass for all GUI components
             */
            @Override
            public void onClick(View v) {
                IdentityModule.getInstance(mCtx).replicateData();
            }
        });

        mClearBtn.setOnClickListener(new View.OnClickListener() {
            /**
             * On click on clear button (present in finger print and eyeris requesting page), clear/remove the finger information and eyeris information and set the background of respective pages.
             * @param v : View class.Responsible for drawing and event handling.The View class is a superclass for all GUI components.
             */
            @Override
            public void onClick(View v) {
                switch (mCurrentState) {
                    case USER_EYE:
                        mLeftEye="";
                        mRightEye="";
                        setEyeBackGround();
                        break;
                    case USER_FINGER_R:
                        mRightIndex="";
                        setRightFingerBackGround();
                        break;
                    case USER_FINGER_L:
                        mLeftIndex="";
                        setLeftFingerBackGround();
                        break;
                    case USER_FACE:
                        mFace = "";
                        mIsFaceGiven = false;
                        setFaceBackGround();
                        mUserFace.setImageBitmap(null);
                        break;
                    default:
                        Log.e("UI Error: ", "invalid state value");
                        break;

                }
            }
        });

        mScanBtn.setOnClickListener(new View.OnClickListener() {
            /**
             * On click of scane button : a async task will run and scane process will starts.
             * @param v : View class Object.
             */
            @Override
            public void onClick(View v) {

                AsyncTask.execute(new Runnable() {

                    @Override
                    public void run() {
                    new Thread(new Runnable() {
                        public void run() {
                            scanButtonProcessing();
                        }
                    }).start();
                    }
                });
            }
        });


        mBackBtn.setOnClickListener(new View.OnClickListener() {

            /**
             * This is Back button click listener method, depends on the current page, it will decide which page should be display to the user and setting the next button properties and also populate the necessary data that is given.
             * @param view : View class object.
             */
            @Override
            public void onClick(View view) {
                setButton(mNextBtn,true);
                switch (mCurrentState){
                    case USER_CONSENT:
                        finish();
                        break;
                    case USER_NAME:
                        mNextBtn.setVisibility(View.VISIBLE);
                        setScreenDataForConsent();
                        setButton(mNextBtn,true);
                        break;
                    case USER_AGE:
                        setScresetScreenDataForNamenDataForName();
                        break;
                    case USER_ID:
                        if(age != 0) {
                            mUserData.setText(Integer.toString(age));
                        } else {
                            mUserData.getText().clear();
                        }
                        setScreenDataForAge();
                        break;
                    case USER_EYE:
                        if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.GENERAL_APP)
                            setScreenForUserID();
                        else if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.BIOMETRIC_ONLY)
                            setScreenDataForConsent();
                        mClearBtn.setVisibility(View.INVISIBLE);
                        break;
                    case USER_FINGER_R:
                        if(SegmentationAndMatching.getInstance(getApplicationContext()).getBiometricIrisLicStatus()){
                            setScreenDataForIris();
                        }else{
                            mCurrentState = USER_EYE;
                            mBackBtn.callOnClick();
                        }
                        break;
                    case USER_FINGER_L:
                        setScreenDataForRightFinger();
                        break;
                    case USER_FACE:
                        setScreenDataForLeftFinger();
                        break;
                    case USER_RESULT:
                        setScreenDataForFace();
                        break;
                    default:
                        break;
                }
            }
        });

        mPreferrenceSettingBtn.setOnClickListener(new View.OnClickListener() {
            /**
             * This is the preferrence setting intent call.
             * @param v
             */
            @Override
            public void onClick(View v) {
                Intent i = new Intent(UserInput.this, MyPreferencesActivity.class);
                //startActivity(i);
                startActivityForResult(i,PREFERENCES);
            }
        });
    }

    private void reloadPreferences()
    {
        final SharedPreferences sharedPreferences = getSharedPreferences();
        deleteHcwOrPatientOnPreferencesSelection(sharedPreferences);
    }

    private void captureAndfetchFinger(boolean left) {
        if (mBiometricClient != null) {
            String capstr = left? SCANE_LEFT_FINGER_PRINT_REQUEST : SCANE_RIGHT_FINGER_PRINT_REQUEST;
            mBiometricClient.sendRequestToBioDevice(capstr);
            mBiometricClient.setFpCaptured(false);
            try {
                TimeUnit.SECONDS.sleep(1);
                captureFingerSpeak(left);
                mBiometricClient.sendRequestToBioDevice(GET_STATUS_REQUEST);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            boolean isFingerPrintActive = false;
            int trials = 0;
            while (!isFingerPrintActive && trials < 30){
                if (mBiometricClient.isFpCaptured()) {
                    Log.d("Bio-status","Recvd data");
                    isFingerPrintActive = true;
                    //TTS.Speak("Finger Captured Successfully");
                    break;
                }
                try {
                    trials++;
                    mBiometricClient.sendRequestToBioDevice(GET_STATUS_REQUEST);
                    if(trials%12 == 0) captureFingerSpeak(left);
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Log.e("Bio-wait error",e.toString());
                }
            }
            if(isFingerPrintActive) {
                Log.d("FingerCaptured", "Try to Get Data");
                boolean isDataRecieved = false;
                trials = 0;
                while (!isDataRecieved && trials < 10){
                    mBiometricClient.setDataReceived(false);
                    String fingerDataString = left? GET_LEFT_FINGER_DATA : GET_RIGHT_FINGER_DATA;
                    mBiometricClient.sendRequestToBioDevice(fingerDataString);
                    try {
                        trials++;
                        TimeUnit.SECONDS.sleep(1);
                        Log.d("Bio-wait","Waiting for biometrics");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        Log.e("Bio-wait error",e.toString());
                    }
                    if (mBiometricClient.ismIsDataRecieved()) {
                        Log.d("Bio-status","Recvd Finger");
                        TTS.Speak("Received Captured Finger Successfully");
                        isDataRecieved = true;
                        mBiometricInformationJson = mBiometricClient.getmBiometricJSON();
                        if(left){
                            mLeftIndex = getImageStringFromJson(mCtx, mBiometricInformationJson,false,true);
                        }else {
                            mRightIndex = getImageStringFromJson(mCtx, mBiometricInformationJson, false, false);
                        }
                    }
                }
            }else{
                TTS.Speak("Error, Unable to Capture or Receive Finger Please try again");
            }
            mBiometricClient.sendRequestToBioDevice(CLOSE_FINGER_PRINT_REQUEST);
        }
    }


    private void setAndGetScreenOnNextButtonClicked() {

        switch (mCurrentState)
        {
            case USER_CONSENT:
                String consent = "";
                if(isHCW()) {
                    consent = "I agree that I am the registered user of this device. I consent that my biometric information may be used for the purposes of validating my identity in my capacity as a HCW";
                    SessionProperties.getInstance(mCtx).setHCWConsent(consent);
                } else {
                    consent = "I agree that my health record may be stored remotely for the purpose of providing a record of my healthcare as well as for providing anonymized statistical information. I consent that my biometric information may be used for the purposes of retriving my record";
                    SessionProperties.getInstance(mCtx).setPatientConsent(consent);
                }
                if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.GENERAL_APP)
                    setScresetScreenDataForNamenDataForName();
                else if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.BIOMETRIC_ONLY)
                {
                    if(SegmentationAndMatching.getInstance(getApplicationContext()).getBiometricIrisLicStatus()){
                        setScreenDataForIris();
                    } else {
                        mCurrentState = USER_EYE;
                        mNextBtn.callOnClick();
                    }
                }
                break;
            case USER_NAME:
                name = mUserData.getText().toString();
                if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.GENERAL_APP)
                    setScreenDataForAge();
                else if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.BIOMETRIC_ONLY)
                {
                    if(SegmentationAndMatching.getInstance(getApplicationContext()).getBiometricIrisLicStatus()){
                        setScreenDataForIris();
                    } else {
                        mCurrentState = USER_EYE;
                        mNextBtn.callOnClick();
                    }
                }

                break;
            case USER_AGE:
                Log.d("----age(Month)----->>" , mAgeMonths.getSelectedItem().toString());
                if(mAgeMonths.getSelectedItem() != null && mAgeYears.getSelectedItem() != null) {
                    Calendar dob = Calendar.getInstance();
                    dob.add(Calendar.YEAR, 0- parseStringToInteger(mAgeYears.getSelectedItem().toString()));
                    dob.add(Calendar.MONTH, 0- parseStringToInteger(mAgeMonths.getSelectedItem().toString()));
                    Calendar now = Calendar.getInstance();
                    age = (int) BioUtils.daysBetween(dob,now);
                }
                setScreenForUserID();
                break;
            case USER_ID:
                if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.GENERAL_APP)
                    uid = mUserData.getText().toString();
                if(SegmentationAndMatching.getInstance(getApplicationContext()).getBiometricIrisLicStatus()){
                    setScreenDataForIris();
                }else {
                    mCurrentState = USER_EYE;
                    mNextBtn.callOnClick();
                }
                break;
            case USER_EYE:
                setScreenDataForRightFinger();
                break;
            case USER_FINGER_R:
                setScreenDataForLeftFinger();
                break;
            case USER_FINGER_L:
                setScreenDataForFace();
                break;
            case USER_FACE:
                long searchStart = System.currentTimeMillis();
                setSessionPropData();
                mClearBtn.setVisibility(View.INVISIBLE);
                if (mFace != null && mFace.length() > 0)
                    mIsFaceGiven = true;
                else
                    mIsFaceGiven = false;
                mBiometricInformationJson = CreateImageMessage(mCtx,mLeftEye,mRightEye,mLeftIndex,mRightIndex,mFace);
                int addRet;
                //bioJSON = BioUtils.CreateTestMessage(mCtx,"/mnt/sdcard/Neurotechnology/leftIris.png","/sdcard/Neurotechnology/rightIris.png","/mnt/sdcard/Neurotechnology/fingerpPrint.png");
                mUserData.getText().clear();
                mUserFace.setVisibility(View.INVISIBLE);
                mScanBtn.setVisibility(View.INVISIBLE);
                StringBuffer guid = new StringBuffer();
                StringBuffer missingParams = new StringBuffer();
                Map<String,String> searchParams = new HashMap<String,String>();
                if (name != null && name.length() != 0)
                    searchParams.put("name", name);
                if (age > 0)
                    searchParams.put("age",Integer.toString(age));
                if (uid != null && uid.length() != 0)
                    searchParams.put("uid",uid);
                if (mBiometricInformationJson != null && mBiometricInformationJson.length() != 0)
                    searchParams.put("BiometricData", mBiometricInformationJson);
                for (Map.Entry<String,String> entrySet : searchParams.entrySet())
                    System.out.println("Key: "+entrySet.getKey()+" Value: "+entrySet.getValue());
                if (searchParams.size() == 0)
                    addRet = -1;
                else {
                    searchParams.put("indType", IdmSessionProperties.getSessionInstance(mCtx).getUserType());
                    addRet = IdentityModule.getInstance(mCtx).isUserPresent(name,searchParams,guid,missingParams);
                }
                mSearchResult.setVisibility(View.VISIBLE);
                if (addRet == 1)
                    Log.d("GUID found: ",guid.toString());
                mBiometricInformationJson = searchParams.get("BiometricData");
                String inputValue="";
                Integer yr = 0;
                Integer mnth = 0;
                int[] ym = BioUtils.ConvertDaysToAge(age);
                yr = ym[0];
                mnth = ym[1];
                String res= "Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;"+ (IdmSessionProperties.getSessionInstance(mCtx).getUserType().equals("HCW")?"HCW":"Patient") +"<br>";
                      if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.GENERAL_APP)
                      {
                          res+= "Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;"+ name  +"<br>";
                          res+= "Age&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;"+ ((yr > 0) ? yr+"y " : " ")+((mnth > 0) ? mnth+"m" : " ") +"<br>";
                      }
                      res+= "Id&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;"+ uid  +"<br>";
                      res+= "Left Finger :&nbsp;"+ (mLeftIndex.length() > 0 ? "Available":"Not Captured")  +"<br>";
                      res+= "Right Finger:&nbsp;"+ (mRightIndex.length() > 0 ? "Available":"Not Captured")  +"<br>";
                      res+= "Left Eye &nbsp;&nbsp;&nbsp;:&nbsp;"+ (mLeftEye.length() > 0 ? "Available":"Not Captured")  +"<br>";
                      res+= "Right Eye &nbsp;&nbsp;:&nbsp;"+ (mRightEye.length() > 0 ? "Available":"Not Captured")  +"<br>";
                      res+= "Face &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;"+ (mFace.length() > 0 ? "Available":"Not Captured")  +"<br>";
                inputValue = res + "<br>";

                if (addRet == 1){
                    Log.d("Search-result","User found");
                    mIsAddUser = false;
                    String sp = IdmSessionProperties.getSessionInstance(mCtx).getUserType()+" Found ";
                    if (isHCW()) {
                        yr = Integer.parseInt(SessionProperties.getInstance(mCtx).getHcwAge())/365;
                        mnth = ((Integer.parseInt(SessionProperties.getInstance(mCtx).getHcwAge()) - (yr*365))/30);
                        SessionProperties.getInstance(mCtx).setHcwGUID(guid.toString());
                        Log.d("Current session GUID: ",SessionProperties.getInstance(mCtx).getHcwGUID());
                        res= "Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;"+ (IdmSessionProperties.getSessionInstance(mCtx).getUserType().equals("HCW")?"HCW":"Patient") +"<br>";
                        if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.GENERAL_APP) {
                            res += "Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;" + SessionProperties.getInstance(mCtx).getHcwName() + "<br>";
                            res += "Age&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;" + ((yr > 0) ? yr + "y " : " ") + ((mnth > 0) ? mnth + "m" : " ") + "<br>";
                        }
                        res+= "Id&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;"+ SessionProperties.getInstance(mCtx).getID(true)  +"<br>";
                        res+= "Left Finger :&nbsp;"+ (mLeftIndex.length() > 0 ? "Available":"Not Captured")  +"<br>";
                        res+= "Right Finger:&nbsp;"+ (mRightIndex.length() > 0 ? "Available":"Not Captured")  +"<br>";
                        res+= "Left Eye &nbsp;&nbsp;&nbsp;:&nbsp;"+ (mLeftEye.length() > 0 ? "Available":"Not Captured")  +"<br>";
                        res+= "Right Eye &nbsp;&nbsp;:&nbsp;"+ (mRightEye.length() > 0 ? "Available":"Not Captured")  +"<br>";
                        res+= "Face &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;"+ (mFace.length() > 0 ? "Available":"Not Captured")  +"<br>";
                        inputValue = res + "<br>";
                    } else {
                        yr = Integer.parseInt(SessionProperties.getInstance(mCtx).getPatientAge())/365;
                        mnth = ((Integer.parseInt(SessionProperties.getInstance(mCtx).getPatientAge()) - (yr*365))/30);
                        SessionProperties.getInstance(mCtx).setPatientGUID(guid.toString());
                        res= "Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;"+ (IdmSessionProperties.getSessionInstance(mCtx).getUserType().equals("HCW")?"HCW":"Patient") +"<br>";
                        if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.GENERAL_APP) {
                            res += "Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;" + SessionProperties.getInstance(mCtx).getPatientName() + "<br>";
                            res += "Age&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;" + ((yr > 0) ? yr + "y " : " ") + ((mnth > 0) ? mnth + "m" : " ") + "<br>";
                        }
                        res+= "Id&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;"+ SessionProperties.getInstance(mCtx).getID(false)  +"<br>";
                        res+= "Left Finger :&nbsp;"+ (mLeftIndex.length() > 0 ? "Available":"Not Captured")  +"<br>";
                        res+= "Right Finger:&nbsp;"+ (mRightIndex.length() > 0 ? "Available":"Not Captured")  +"<br>";
                        res+= "Left Eye &nbsp;&nbsp;&nbsp;:&nbsp;"+ (mLeftEye.length() > 0 ? "Available":"Not Captured")  +"<br>";
                        res+= "Right Eye &nbsp;&nbsp;:&nbsp;"+ (mRightEye.length() > 0 ? "Available":"Not Captured")  +"<br>";
                        res+= "Face &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;"+ (mFace.length() > 0 ? "Available":"Not Captured")  +"<br>";
                        inputValue = res + "<br>";
                    }
                    mSearchResult.setText(Html.fromHtml(inputValue + sp));
                    mImageResult.setVisibility(View.VISIBLE);
                    String base64Str = "";
                    if (isHCW()) {
                        base64Str = SessionProperties.getInstance(mCtx).getHCWPhotoStr();
                    //    Log.d("HCW-photo",base64Str.toString());
                    } else {
                        base64Str = SessionProperties.getInstance(mCtx).getPatientPhotoStr();
                     //   Log.d("Patient-photo",base64Str.toString());
                    }
                    if (base64Str != null && base64Str.length() > 0) {
                        if (base64Str.equals("null"))
                            Log.e("Photo-error","Photo is null");
                        byte[] imgBytes = Base64.decode(base64Str, 0);
                        Bitmap personImg = BitmapFactory.decodeByteArray(imgBytes, 0, imgBytes.length);
                        mImageResult.setImageBitmap(personImg);
                    }
                    else
                        Log.e("Photo-error","Could not get photo");
                    mScanBtn.setVisibility(View.INVISIBLE);
                    mPersonName.setVisibility(View.VISIBLE);
                    if (IdmSessionProperties.getSessionInstance(mCtx).getUserType().equals("HCW")) {
                        sp +=SessionProperties.getInstance(mCtx).getHcwName();
                    } else {
                        sp +=SessionProperties.getInstance(mCtx).getPatientName();
                    }

                    TTS.Speak(sp);
                }
                else if (addRet == 0 || addRet == -3) {
                    Log.d("Search-result","User not found");
                    mIsAddUser = true;
                    String sp="Patient not found due to one of:\n <br/>" +
                            "\n" +
                            "1) New patient encounter at this or other clinics\n <br/>" +
                            "\n" +
                            "2) Not enough information was entered\n <br/>" +
                            "\n" +
                            "3) Information entered had errors";
                    TTS.Speak(IdmSessionProperties.getSessionInstance(mCtx).getUserType()+" not found");
                    mSearchResult.setText(Html.fromHtml(inputValue + sp));
                    if (addRet == -3) {
                        mIsNoImpDataProvided = true;
                        Log.d("Search-result","some imp data not given");
                    }
                }
                else if (addRet ==  -1) {
                    //no info given
                    Log.d("No users found","Pls Update/add info");
                    TTS.Speak("No users found. Update/add info");
                    mSearchResult.setText(Html.fromHtml(inputValue + "No users found. Update/add info."));
                    setButton(mNextBtn,false);
                } else if (addRet == -4) {
                    //multiple users
                    Log.d("Multiple users found"," Pls Update/add info");
                    TTS.Speak("Multiple users found. Update/add info");
                    mSearchResult.setText(Html.fromHtml(inputValue + "Multiple users found. Update/add info."));
                    mIsMultipleResFound = true;
                    mIsAddUser = true;
                    setButton(mNextBtn,true);
                }
                else
                {
                    mIsNoImpDataProvided = true;
                    mIsAddUser = true;
                    setButton(mNextBtn,true);
                    TTS.Speak("Internal error. Please recheck your data");
                    mSearchResult.setText(Html.fromHtml(inputValue + "Internal error. please recheck data or try later"));
                }
                mUserFingerL.setVisibility(View.INVISIBLE);
                mUserData.setVisibility(View.INVISIBLE);
                mConsentCheckBoxDeclaration1.setVisibility(View.INVISIBLE);
                mConsentCheckBoxDeclaration2.setVisibility(View.INVISIBLE);
                mUserTxt.setText("Search Results");
                mCurrentState = USER_RESULT;
                long searchEnd = System.currentTimeMillis();
                System.out.println("End to end time for search: "+(searchEnd-searchStart));
                break;
            case USER_RESULT:
                long addStart = System.currentTimeMillis();
                final StringBuffer guiddata = new StringBuffer();
                if (mIsAddUser) {
                    if (mIsNoImpDataProvided || mIsMultipleResFound)
                    {
                        AlertDialog.Builder addCheckDialog = new AlertDialog.Builder(UserInput.this);
                        addCheckDialog.setMessage("Not enough parameters. You really wanna add?");
                        addCheckDialog.setTitle("Need more data");
                        addCheckDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                boolean ret = IdentityModule.getInstance(mCtx).addIndividual(guiddata, name, uid, gid, age, "", "", "", IdmSessionProperties.getSessionInstance(mCtx).getUserType(), "", "", mBiometricInformationJson);
                                Log.d("Search Results", ret ? "Added Individual successfully" : "Add Individual Failure");
                                loginUser(guiddata.toString(),IdmSessionProperties.getSessionInstance(mCtx).getUserType());
                                cameraIntentForUsers();
                            }
                        });
                        addCheckDialog.setNegativeButton("No", null);
                        AlertDialog addAlert = addCheckDialog.create();
                        addAlert.show();
                    }
                    else
                    {
                        boolean ret = IdentityModule.getInstance(mCtx).addIndividual(guiddata, name, uid, gid, age, "", "", "", IdmSessionProperties.getSessionInstance(mCtx).getUserType(), "", "", mBiometricInformationJson);
                        Log.d("Search Results", ret ? "Added Individual successfully" : "Add Individual Failure");
                        loginUser(guiddata.toString(),IdmSessionProperties.getSessionInstance(mCtx).getUserType());
                        cameraIntentForUsers();
                    }
                    long addEnd = System.currentTimeMillis();
                    System.out.println("Time taken to add: "+(addEnd-addStart));
                } else
                {
                    String indType = IdmSessionProperties.getSessionInstance(mCtx).getUserType();
                    if (indType.equals("HCW"))
                        loginUser(SessionProperties.getInstance(mCtx).getHcwGUID(),indType);
                    else
                        loginUser(SessionProperties.getInstance(mCtx).getPatientGUID(),indType);
                    cameraIntentForUsers();
                }
                break;
            default:
                break;
        }
    }

    private boolean loginUser(String guid,String indType)
    {
        //add transaction for the user indicating user login
        Map<String,String> updateParams = new HashMap<String,String>();
        updateParams.put("indType",indType);
        updateParams.put("guid",guid);
        if (mIsFaceGiven)
            updateParams.put("photo", mFace.toString());
        return IdentityModule.getInstance(mCtx).updateIndividual(updateParams,"Login");
    }


    private void scanButtonProcessing() {
        if ((mCurrentState == USER_FINGER_R || mCurrentState == USER_FINGER_L) && (!SegmentationAndMatching.getInstance(getApplicationContext()).getBiometricFPLicStatus()))
        {
            showToast("Unable to capture Neuro Fingerprint License");
            return ;
        }

        if ((mCurrentState == USER_FACE) && (!SegmentationAndMatching.getInstance(getApplicationContext()).getFaceLicStatus()))
        {
            showToast("Unable to capture Neuro Face License");
            return ;
        }

        if((mCurrentState == USER_EYE)) {
            if(mBiometricClient == null) {
                showToast("Unable to connect to Scanner please check settings");
                return;
            }
            if (!mBiometricClient.isConnected()) {
                showToast("Unable to connect to Scanner device");
                return;
            }
        }
        showProgressBar(true);
        biometricScanningDeviceSelectionAndProcessing();
        showProgressBar(false);
    }


    private void biometricScanningDeviceSelectionAndProcessing() {
        switch (mCurrentState)
        {
            case USER_EYE:
                mLeftEye="";
                mRightEye="";
                setEyeBackGround();
                disableNextAndBackButton(false);
                CaptureAndfetchEyes();
                setEyeBackGround();
                disableNextAndBackButton(true);
                break;
            case USER_FINGER_R:
            case USER_FINGER_L:
                if (mCurrentState == USER_FINGER_R) {
                    mRightIndex = "";
                    setRightFingerBackGround();
                }else{
                    mLeftIndex = "";
                    setLeftFingerBackGround();
                }
                disableNextAndBackButton(false);
                if(mIsEnabledWatsonminiPreference){
                    if(SegmentationAndMatching.getInstance(mCtx).getNumberOfScanner() > 0) {
                        if (mCurrentState == USER_FINGER_R) {
                            captureFingerSpeak(false);
                        } else {
                            captureFingerSpeak(true);
                        }

                        CaptureFingersFromWatsonMini("capture");
                        if (mFilenameforFingerCapture.length() > 0) {
                            if (mCurrentState == USER_FINGER_R) {
                                mRightIndex = mFilenameforFingerCapture;
                            } else {
                                mLeftIndex = mFilenameforFingerCapture;
                            }
                        }
                    }
                }else {
                    CaptureFingerfromCredenceID();
                }
                disableNextAndBackButton(true);
                break;
            case USER_FACE:
                setFaceBackGround();
                CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
                String  camid = getFrontFacingCameraId(manager);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.putExtra("android.intent.extras.CAMERA_FACING",camid);
                Uri photoUri = FileProvider.getUriForFile(UserInput.this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        mPhoto);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(intent, TAKE_PICTURE);
            case USER_RESULT:
                break;
            default:
                Log.e("Userinput screen error","Invalid button click");
                break;
        }
    }
    private int progressStatus = 0;


    private void CaptureAndfetchEyes() {
        if (mBiometricClient != null ) {
            progressStatus = 0;
            mBiometricClient.sendRequestToBioDevice(SCANE_IRIS_REQUEST);
            mBiometricClient.setIrisCaptured(false);
            try {
                SetProgressState(progressStatus+=2);
                TimeUnit.SECONDS.sleep(2);
                TTS.Speak("Please lift the Iris Scanner and show eyes");
                mBiometricClient.sendRequestToBioDevice(GET_STATUS_REQUEST);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            boolean eyeActive = false;
            int trials = 0;
            while ((!eyeActive) && (trials < 30)){
                if (mBiometricClient.isIrisCaptured()) {
                    Log.d("Bio-status","Recvd data");
                    eyeActive = true;
                    //TTS.Speak("Eyes Captured Successfully");
                    break;
                }
                try {
                    trials++;
                    mBiometricClient.sendRequestToBioDevice(GET_STATUS_REQUEST);
                    SetProgressState(progressStatus+=1);
                    if(trials%12 == 0)TTS.Speak("Please lift the Iris Scanner and show eyes");
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Log.e("Bio-wait error",e.toString());
                }
            }
            if(eyeActive) {
                Log.d("IrisCaptured", "Try to Get Data");
                boolean dataRecvd = false;
                trials = 0;
                while (!dataRecvd && trials < 10){
                    mBiometricClient.setDataReceived(false);
                    mBiometricClient.sendRequestToBioDevice(GET_IRIS_REQUEST);
                    try {
                        trials++;
                        SetProgressState(progressStatus+=1);
                        TimeUnit.SECONDS.sleep(1);
                        Log.d("Bio-wait","Waiting for biometrics");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        Log.e("Bio-wait error",e.toString());
                    }
                    if (mBiometricClient.ismIsDataRecieved()) {
                        TTS.Speak("Received Captured Eyes Successfully");
                        Log.d("Bio-status","Recvd Eyes");
                        dataRecvd = true;
                        mBiometricInformationJson = mBiometricClient.getmBiometricJSON();
                        mLeftEye = getImageStringFromJson(mCtx, mBiometricInformationJson,true,true);
                        mRightEye = getImageStringFromJson(mCtx, mBiometricInformationJson,true,false);
                    }
                }
            }else{
                TTS.Speak("Error, Unable to Capture or Receive Eyes Please try again");
            }
            mBiometricClient.sendRequestToBioDevice(CLOSE_IRIS_REQUEST);
        }
    }

    private String getFrontFacingCameraId(CameraManager cManager) {
        String cameraId="0";
        try {
            cameraId=  cManager.getCameraIdList()[1];
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        return cameraId;
    }

    private void CaptureFingerfromCredenceID() {
        if (mBiometricClient != null){
            mBiometricClient.sendRequestToBioDevice(OPEN_FINGER_PRINT_REQUEST);
            try {
                TimeUnit.SECONDS.sleep(1);
                mBiometricClient.sendRequestToBioDevice(GET_STATUS_REQUEST);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //check if fingerprint scanner has been opened
            boolean fpActive = false;
            int trials = 0;
            while (!fpActive && trials < 7) {
                try {
                    trials++;
                    TimeUnit.SECONDS.sleep(1);
                    Log.d("Bio-wait","Waiting for FP sensor to open");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Log.e("Bio-wait error",e.toString());
                }
                if (mBiometricClient.isFPSensorActive()) {
                    Log.d("Bio-status","Recvd data");
                    fpActive = true;
                }
                else
                    mBiometricClient.sendRequestToBioDevice(GET_STATUS_REQUEST);
            }
            Log.d("FP now active","start scan");
            //if you are her it means FP sensor is active. ok to send scan req
            if (mCurrentState == USER_FINGER_R) {
                captureAndfetchFinger(false);
                setRightFingerBackGround();
            }else {
                captureAndfetchFinger(true);
                setLeftFingerBackGround();
            }
        }
    }


    private void SetProgressState(final int ps) {
//                handler.post(new Runnable() {
//                    public void run() {
//                        mProgressBar.setProgress(ps);
//                    }
//                });
    }

    /**
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("onActivityResult: ", Integer.toString(requestCode)+"::"+Integer.toString(resultCode));
        if (resultCode == RESULT_OK) {
            if (requestCode == CAPTURE_FINGER) {

                String init = data.getStringExtra("init");
                Log.d("Madhav","init data " +((init != null)?init:"NULL"));
                if((init != null) && init.contains("success")){
                    mIsEnabledWatsonminiPreference = true;
                    return;
                }
                String captureFromRecievecIntent = data.getStringExtra("capture");
                Log.d("Madhav","capture data " +((captureFromRecievecIntent != null)?captureFromRecievecIntent:"NULL"));
                if((captureFromRecievecIntent != null) && captureFromRecievecIntent.contains("success")){
                    String path = data.getStringExtra("image");
                    if(path != null && path.length()> 0) {
                        mFilenameforFingerCapture = path;
                        String imgstr = BioUtils.ReadFileToBytes(mCtx,mFilenameforFingerCapture);
                        Log.d("Madhav","FileSize of cap image in base64 is " +imgstr.length());
                        if (mCurrentState == USER_FINGER_R) {
                            mRightIndex = imgstr;
                            setRightFingerBackGround();
                        }else {
                            mLeftIndex = imgstr;
                            setLeftFingerBackGround();
                        }
                    }
                }
            } else if (requestCode == PREFERENCES) {
                //we got out of preferences screen. now reload preferences
                reloadPreferences();
            } else if (requestCode == TAKE_PICTURE) {
                String path = Environment.getExternalStorageDirectory()+"/"+getResources().getString(R.string.HCWPicName);
                Bitmap camImage = ImageUtils.readImageAndAlignExif(path);
                BioUtils.SaveJpegImage(camImage,path);
                camImage = BioUtils.resizeBitmap(path,720,1280);
                BioUtils.SaveJpegImage(camImage,path);
                mUserFace.setImageBitmap(camImage);
                mFace = BioUtils.ReadFileToBytes(mCtx,path);
                setFaceBackGround();
                //mNextBtn.setBackground(getResources().getDrawable(R.drawable.blue_rounded_background,null));
                mNextBtn.setEnabled(true);
            }
        }
    }


    private void cameraIntentForUsers() {
        if (mIsFaceGiven)
        {
            setResult(RESULT_OK);
            finish();
            return;
        }
        Intent i;
        if (IdmSessionProperties.getSessionInstance(mCtx).getUserType().equals("HCW"))
            i = new Intent(UserInput.this, HCWCamera.class);
        else
            i = new Intent(UserInput.this, PatientCamera.class);

        i.putExtras(getIntent());
        i.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
        startActivity(i);
        finish();
    }


    private void idmLandingPageSelection() {

        switch (mCurrentState)
        {
            case USER_CONSENT:
                mUserTxt.setText(IdmSessionProperties.getSessionInstance(mCtx).getUserType()+"  Consent");
                String toast="";
                if(!SegmentationAndMatching.getInstance(getApplicationContext()).getBiometricFPLicStatus()){
                    toast = "Unable to capture Neuro Finger License" + "\n";
                }
                if(!SegmentationAndMatching.getInstance(getApplicationContext()).getBiometricIrisLicStatus()){
                    toast += "Unable to capture Neuro Iris License";
                }
                if(toast.length()> 0)showToast(toast);
                setScreenDataForConsent();
                mCurrentState = UserDataType.USER_CONSENT;
                break;
            case USER_NAME:
                mUserTxt.setText(IdmSessionProperties.getSessionInstance(mCtx).getUserType()+"  Name");
                mUserData.setHint("Enter name of  "+IdmSessionProperties.getSessionInstance(mCtx).getUserType());
                break;
            case USER_AGE:
                mUserTxt.setText(IdmSessionProperties.getSessionInstance(mCtx).getUserType()+"  Age");
                mUserData.setHint("Enter age of  "+IdmSessionProperties.getSessionInstance(mCtx).getUserType());
                break;
            case USER_ID:
                mUserTxt.setText(IdmSessionProperties.getSessionInstance(mCtx).getUserType()+"  ID");
                mUserData.setHint("Enter ID of  "+IdmSessionProperties.getSessionInstance(mCtx).getUserType());
                break;
            case GOV_ID:
                mUserTxt.setText(IdmSessionProperties.getSessionInstance(mCtx).getUserType()+" Gov ID");
                mUserData.setHint("Enter Gov ID of  "+IdmSessionProperties.getSessionInstance(mCtx).getUserType());
                break;
            case USER_EYE:
                mUserTxt.setText(IdmSessionProperties.getSessionInstance(mCtx).getUserType()+"  Eyes");
                mScanBtn.setText("Iris scan");
                mScanBtn.setVisibility(View.VISIBLE);
                mUserData.setHint("Please give eye scan for  "+IdmSessionProperties.getSessionInstance(mCtx).getUserType());
                break;
            case USER_FINGER_R:
                mUserTxt.setText(IdmSessionProperties.getSessionInstance(mCtx).getUserType()+"  right Fingerprint");
                mScanBtn.setText("Right Fingerprint scan");
                mScanBtn.setVisibility(View.VISIBLE);
                mUserFingerR.setVisibility(View.VISIBLE);
                break;
            case USER_FINGER_L:
                mUserTxt.setText(IdmSessionProperties.getSessionInstance(mCtx).getUserType()+"  left Fingerprint");
                mScanBtn.setText("Left Fingerprint scan");
                mScanBtn.setVisibility(View.VISIBLE);
                mUserFingerL.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }

    private void deleteHcwOrPatientOnPreferencesSelection(SharedPreferences sharedPreferences) {
        boolean isClearHWC = sharedPreferences.getBoolean("Clear HCW",false);
        boolean isClearPatient = sharedPreferences.getBoolean("Clear Patient",false);

        if (isClearHWC || isClearPatient) {
            IdentityModule.getInstance(mCtx).deleteIndividualFromDB(getMapUserTypeToBeDeleteFromDb(isClearHWC, isClearPatient));
            if (isClearHWC) {
                SessionProperties.getInstance(mCtx).clearHCW();
                clearHwcOrPatientPreferrenceSelection(sharedPreferences, "Clear HCW");
            }
            if (isClearPatient) {
                SessionProperties.getInstance(mCtx).clearPatient();
                clearHwcOrPatientPreferrenceSelection(sharedPreferences, "Clear Patient");
            }
        }
    }

    private void clearHwcOrPatientPreferrenceSelection(SharedPreferences prefs, String s) {
        Log.d(s, "clear (Hwc Or Patient) Preferrence Selection DONE");
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(s, false);
        editor.commit();
    }

    @NotNull
    private Map<String, String> getMapUserTypeToBeDeleteFromDb(boolean clearHWC, boolean clearPatient) {

        Map<String,String> userTypeMap = new HashMap<>();
        if (clearHWC && clearPatient){
            userTypeMap.put("indType", "All");
        }else{
            if (clearHWC){
                userTypeMap.put("indType", "HCW");
            }
            if (clearPatient){
                userTypeMap.put("indType", "Patient");
            }
        }
        return userTypeMap;
    }

    @NotNull
    private SharedPreferences getSharedPreferences() {
        long inSharedPref = System.currentTimeMillis();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(UserInput.this);

        String remote_ip_address = sharedPreferences.getString("remote ip","192.168.43.174");
        IdentityModule.getInstance(mCtx).initReplicator(remote_ip_address, mCtx);

        String watsonMini_or_credenceId_selection = sharedPreferences.getString("listpref", "Watson Mini");
        mIsEnabledWatsonminiPreference = (watsonMini_or_credenceId_selection.equalsIgnoreCase("Watson Mini"));
        long outSharedPref = System.currentTimeMillis();
        System.out.println("Time in shared preference: "+(outSharedPref-inSharedPref));
        return sharedPreferences;
    }

    private void userAgeDropDownSelectionInitialization(int height) {
        try {
            Field declaredFieldForAgePopup = Spinner.class.getDeclaredField("mPopup");
            declaredFieldForAgePopup.setAccessible(true);
            android.widget.ListPopupWindow popupWindowAgeMonths = (android.widget.ListPopupWindow) declaredFieldForAgePopup.get(mAgeMonths);
            popupWindowAgeMonths.setHeight(height);
            android.widget.ListPopupWindow popupWindowAgeYears = (android.widget.ListPopupWindow) declaredFieldForAgePopup.get(mAgeYears);
            popupWindowAgeYears.setHeight(height);
        }
        catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            // silently fail...
            Log.d("age list", e.toString());
        }
    }

    private void setSessionPropData() {
        if (IdmSessionProperties.getSessionInstance(mCtx).getUserType().equals("HCW")) {
            SessionProperties.getInstance(mCtx).setHcwName(name);
            SessionProperties.getInstance(mCtx).setHcwAge(age);
            SessionProperties.getInstance(mCtx).setID(true,uid);
        }
        else {
            SessionProperties.getInstance(mCtx).setPatientName(name);
            SessionProperties.getInstance(mCtx).setPatientAge(age);
            SessionProperties.getInstance(mCtx).setID(false,uid);
        }
    }

    private void setScreenDataForFace() {
        if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.BIOMETRIC_ONLY)
        {
            mConsentCheckBoxDeclaration1.setVisibility(View.INVISIBLE);
            mConsentCheckBoxDeclaration2.setVisibility(View.INVISIBLE);
        }
        mSearchResult.setVisibility(View.INVISIBLE);
        mImageResult.setVisibility(View.INVISIBLE);
        mPersonName.setVisibility(View.INVISIBLE);
        mUserData.getText().clear();
        mUserFingerR.setVisibility(View.INVISIBLE);
        mUserTxt.setText(IdmSessionProperties.getSessionInstance(mCtx).getUserType()+"  Face");
        mUserFace.setVisibility(View.VISIBLE);
        mUserData.setVisibility(View.INVISIBLE);
        mScanBtn.setText("Face scan");
        mScanBtn.setVisibility(View.VISIBLE);
        mUserFingerL.setVisibility(View.INVISIBLE);
        //setLeftFingerBackGround();
        mCurrentState = UserDataType.USER_FACE;
        mClearBtn.setVisibility(View.VISIBLE);
        mIsNoImpDataProvided = false;
        mIsMultipleResFound = false;
        checkNeuroFaceLicense();
        setFaceBackGround();
    }

    private void setScreenDataForLeftFinger() {
        if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.BIOMETRIC_ONLY)
        {
            mConsentCheckBoxDeclaration1.setVisibility(View.INVISIBLE);
            mConsentCheckBoxDeclaration2.setVisibility(View.INVISIBLE);
        }
        mSearchResult.setVisibility(View.INVISIBLE);
        mImageResult.setVisibility(View.INVISIBLE);
        mPersonName.setVisibility(View.INVISIBLE);
        mUserData.getText().clear();
        mUserFingerR.setVisibility(View.INVISIBLE);
        mUserFace.setVisibility(View.INVISIBLE);
        mUserTxt.setText(IdmSessionProperties.getSessionInstance(mCtx).getUserType()+"  Left Fingerprint");
        mUserData.setVisibility(View.INVISIBLE);
        mScanBtn.setText("Left Fingerprint scan");
        mScanBtn.setVisibility(View.VISIBLE);
        mUserFingerL.setVisibility(View.VISIBLE);
        mUserFingerL.setImageResource(R.drawable.palmindex);
        setLeftFingerBackGround();
        mCurrentState = UserDataType.USER_FINGER_L;
        mClearBtn.setVisibility(View.VISIBLE);
        mIsNoImpDataProvided = false;
        mIsMultipleResFound = false;
        checkNeuroLicAndDevice();
    }

    private void setScreenDataForRightFinger() {
        mUserData.getText().clear();
        if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.BIOMETRIC_ONLY)
        {
            mConsentCheckBoxDeclaration1.setVisibility(View.INVISIBLE);
            mConsentCheckBoxDeclaration2.setVisibility(View.INVISIBLE);
        }
        mUserEyes.setVisibility(View.INVISIBLE);
        mUserTxt.setText(IdmSessionProperties.getSessionInstance(mCtx).getUserType()+"  Right Fingerprint");
        mUserData.setVisibility(View.INVISIBLE);
        mScanBtn.setText("Right Fingerprint scan");
        mScanBtn.setVisibility(View.VISIBLE);
        setRightFingerBackGround();
        mUserFingerL.setVisibility(View.INVISIBLE);
        mUserFingerR.setVisibility(View.VISIBLE);
        mUserFingerR.setImageResource(R.drawable.palmindexright);
        mCurrentState = USER_FINGER_R;
        mClearBtn.setVisibility(View.VISIBLE);
        checkNeuroLicAndDevice();
    }

    private void checkNeuroLicAndDevice() {
        String toast="";
        if(!SegmentationAndMatching.getInstance(getApplicationContext()).getBiometricFPLicStatus()){
            toast = "Unable to capture Neuro Fingerprint License" + "\n";
        }
        if(!(SegmentationAndMatching.getInstance(mCtx).getNumberOfScanner() > 0)){
            toast+="Unable to detect FingerPrint devices";
        }
        if(toast.length()>0)showToast(toast);
    }


    private void checkNeuroFaceLicense()
    {
        String toast="";
        if(!SegmentationAndMatching.getInstance(getApplicationContext()).getFaceLicStatus()){
            toast = "Unable to capture Neuro Face License" + "\n";
        }
        if(toast.length()>0)showToast(toast);
    }

    private void setScreenDataForIris() {
        if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.BIOMETRIC_ONLY)
        {
            mConsentCheckBoxDeclaration1.setVisibility(View.INVISIBLE);
            mConsentCheckBoxDeclaration2.setVisibility(View.INVISIBLE);
        }
        mUserTxt.setText(IdmSessionProperties.getSessionInstance(mCtx).getUserType()+"  Eyes");
        mUserData.getText().clear();
        mUserData.setVisibility(View.INVISIBLE);
        mUserFingerL.setVisibility(View.INVISIBLE);
        mUserFingerR.setVisibility(View.INVISIBLE);
        mScanBtn.setText("Iris scan");
        mScanBtn.setVisibility(View.VISIBLE);
        mUserEyes.setVisibility(View.VISIBLE);
        mUserEyes.setImageResource(R.drawable.eyes1);
        setEyeBackGround();
        if(mBiometricClient != null) mBiometricClient.sendRequestToBioDevice("Init");
        mCurrentState = UserDataType.USER_EYE;
        mUserFingerR.setVisibility(View.INVISIBLE);
        mClearBtn.setVisibility(View.VISIBLE);
    }

    private void setScreenForUserID() {
        mUserTxt.setText((IdmSessionProperties.getSessionInstance(mCtx).getUserType().equals("HCW")?"HCW":"MAMTA") +"  ID");
        mUserData.setText(uid);
        mUserData.setHint("Enter ID of "+ IdmSessionProperties.getSessionInstance(mCtx).getUserType());
        mCurrentState = UserDataType.USER_ID;
        mUserData.setVisibility(View.VISIBLE);
        mUserFingerR.setVisibility(View.INVISIBLE);
        mUserFingerL.setVisibility(View.INVISIBLE);
        mUserEyes.setVisibility(View.INVISIBLE);
        mScanBtn.setVisibility(View.INVISIBLE);
        mConsentCheckBoxDeclaration1.setVisibility(View.INVISIBLE);
        mConsentCheckBoxDeclaration2.setVisibility(View.INVISIBLE);
        mCurrentState = UserDataType.USER_ID;
        mAgeLinearYear.setVisibility(View.INVISIBLE);
        mAgeLinearMonth.setVisibility(View.INVISIBLE);
        mYears.setVisibility(View.INVISIBLE);
        mMonths.setVisibility(View.INVISIBLE);
    }

    private void setScreenDataForAge() {
        mUserData.getText().clear();
        mUserTxt.setText(IdmSessionProperties.getSessionInstance(mCtx).getUserType()+"  Age");
        mUserData.setHint("Enter age of "+IdmSessionProperties.getSessionInstance(mCtx).getUserType());
        mConsentCheckBoxDeclaration1.setVisibility(View.INVISIBLE);
        mConsentCheckBoxDeclaration2.setVisibility(View.INVISIBLE);
        if(age != 0) {
            mUserData.setText(Integer.toString(age));
        }else {
            mUserData.getText().clear();
        }
        mUserData.setHint("Enter age of "+IdmSessionProperties.getSessionInstance(mCtx).getUserType()+ (isHCW() ?" in years":" in months"));
        mCurrentState = UserDataType.USER_AGE;

        mYears.setVisibility(View.VISIBLE);

        mAgeYears.setVisibility(View.VISIBLE);
        mUserData.setVisibility(View.INVISIBLE);
        mAgeLinearYear.setVisibility(View.VISIBLE);
        mAgeLinearMonth.setVisibility(View.VISIBLE);
        //if hcw then year false
        if(IdmSessionProperties.getSessionInstance(mCtx).getUserType().equals("HCW")){

            mMonths.setVisibility(View.INVISIBLE);
            mAgeMonths.setVisibility(View.INVISIBLE);
        }else{

            mMonths.setVisibility(View.VISIBLE);
            mAgeMonths.setVisibility(View.VISIBLE);
        }
    }

    private void setScresetScreenDataForNamenDataForName() {
        mConsentCheckBoxDeclaration1.setVisibility(View.INVISIBLE);
        mConsentCheckBoxDeclaration2.setVisibility(View.INVISIBLE);
        mUserTxt.setText(IdmSessionProperties.getSessionInstance(mCtx).getUserType()+"  Name");
        mUserData.setText(name);
        mUserData.setHint("Enter name of  "+IdmSessionProperties.getSessionInstance(mCtx).getUserType());
        mUserData.setVisibility(View.VISIBLE);
        mCurrentState = UserDataType.USER_NAME;
        mPreferrenceSettingBtn.setVisibility(View.INVISIBLE);
        mAgeYears.setVisibility(View.INVISIBLE);
        mAgeLinearYear.setVisibility(View.INVISIBLE);
        mAgeLinearMonth.setVisibility(View.INVISIBLE);
        mAgeMonths.setVisibility(View.INVISIBLE);
        mYears.setVisibility(View.INVISIBLE);
        mMonths.setVisibility(View.INVISIBLE);

    }

    private void setScreenDataForConsent() {
        if(isHCW()) {
            mConsentCheckBoxDeclaration1.setText("I agree that I am the registered user of this device");
            mConsentCheckBoxDeclaration2.setText("I consent that my biometric information may be used for the purposes of validating my identity in my capacity as a HCW");
        }else{
            mConsentCheckBoxDeclaration1.setText("I agree that my health record may be stored remotely for the purpose of providing a record of my healthcare as well as for providing anonymized statistical information");
            mConsentCheckBoxDeclaration2.setText("I consent that my biometric information may be used for the purposes of retriving my record");
        }
        mConsentCheckBoxDeclaration1.setVisibility(View.VISIBLE);
        mConsentCheckBoxDeclaration2.setVisibility(View.VISIBLE);
        //mNextBtn.setVisibility((mIsConsentCheckBox1 && mIsConsentCheckBox2)?View.VISIBLE:View.INVISIBLE);
        if (mConsentCheckBoxDeclaration1.isChecked() && mConsentCheckBoxDeclaration2.isChecked())
            setButton(mNextBtn,true);
        else
            setButton(mNextBtn,false);

        if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.BIOMETRIC_ONLY)
        {
            mUserFingerR.setVisibility(View.INVISIBLE);
            mUserFingerL.setVisibility(View.INVISIBLE);
            mUserEyes.setVisibility(View.INVISIBLE);
            mScanBtn.setVisibility(View.INVISIBLE);
        }
        mUserTxt.setText(IdmSessionProperties.getSessionInstance(mCtx).getUserType()+"  Consent");
        mUserData.setVisibility(View.INVISIBLE);
        mClearBtn.setVisibility(View.INVISIBLE);
        mCurrentState = UserDataType.USER_CONSENT;
        mPreferrenceSettingBtn.setVisibility(View.VISIBLE);
        mAgeLinearYear.setVisibility(View.INVISIBLE);
        mAgeLinearMonth.setVisibility(View.INVISIBLE);
        mYears.setVisibility(View.INVISIBLE);
        mMonths.setVisibility(View.INVISIBLE);
    }

    private void captureFingerSpeak(boolean left) {
        if (left) {
            TTS.Speak("Place Left Index finger ON Finger scanner.");
        } else {
            TTS.Speak("Place Right Index finger ON Finger scanner.");
        }
    }

    private void showProgressBar(final boolean b) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.i("Value of display flag: ",Boolean.toString(b));
                mCyclicProgressBar.setVisibility(b?View.VISIBLE:View.INVISIBLE);
            }
        });
    }

    private void showToast(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(mCtx, msg, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setButton(Button b1, boolean b) {
        b1.setEnabled(b);
        b1.setClickable(b);
        b1.setBackgroundResource(b?R.drawable.blue_rounded_background:R.drawable.grey_rounded_background);
    }

    private void disableNextAndBackButton(final boolean b) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setButton(mNextBtn,b);
                setButton(mBackBtn,b);
                setButton(mScanBtn,b);
            }
        });
    }

    private void setEyeBackGround() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mRightEye.length() > 0 || mLeftEye.length() > 0) {
                    mUserEyes.setBackgroundColor(Color.parseColor("#00FF00"));
                } else {
                    mUserEyes.setBackgroundColor(Color.parseColor("#FF0000"));
                }
            }
        });
    }

    private void setLeftFingerBackGround() {
        runOnUiThread(new Runnable() {
            /**
             *
             */
            @Override
            public void run() {
                if (mLeftIndex.length() > 0) {
                    mUserFingerL.setBackgroundColor(Color.parseColor("#00FF00"));
                } else {
                    mUserFingerL.setBackgroundColor(Color.parseColor("#FF0000"));
                }
            }
        });

    }

    private void setRightFingerBackGround() {
        runOnUiThread(new Runnable() {
            /**
             *
             */
            @Override
            public void run() {
                if(mRightIndex.length()>0) {
                    mUserFingerR.setBackgroundColor(Color.parseColor("#00FF00"));
                }else{
                    mUserFingerR.setBackgroundColor(Color.parseColor("#FF0000"));
                }
            }
        });
    }

    private void setFaceBackGround() {
        runOnUiThread(new Runnable() {
            /**
             *
             */
            @Override
            public void run() {
                if(mFace.length()>0) {
                    mUserFace.setBackgroundColor(Color.parseColor("#00FF00"));
                }else{
                    mUserFace.setBackgroundColor(Color.parseColor("#FF0000"));
                }
            }
        });
    }

    private String getValueOfProperty(String property)
    {
        switch (property)
        {
            case "Name":
                if (IdmSessionProperties.getSessionInstance(mCtx).getUserType().equals("HCW")) {
                    if (!TextUtils.isEmpty(SessionProperties.getInstance(mCtx).getHcwName()))
                        return SessionProperties.getInstance(mCtx).getHcwName();
                } else
                {
                    if (!TextUtils.isEmpty(SessionProperties.getInstance(mCtx).getPatientName()))
                        return SessionProperties.getInstance(mCtx).getPatientName();
                }
                break;
            case "Age":
                if (IdmSessionProperties.getSessionInstance(mCtx).getUserType().equals("HCW")) {
                    if (!TextUtils.isEmpty(SessionProperties.getInstance(mCtx).getHcwAge()))
                        return SessionProperties.getInstance(mCtx).getHcwAge();
                } else
                {
                    if (!TextUtils.isEmpty(SessionProperties.getInstance(mCtx).getPatientAge()))
                        return SessionProperties.getInstance(mCtx).getPatientAge();
                }
                break;
            case "UserID":
                if (!TextUtils.isEmpty(SessionProperties.getInstance(mCtx).getID(isHCW())))
                        return SessionProperties.getInstance(mCtx).getID(isHCW());
               break;
            default:
                break;
        }
        return "";
    }

    private boolean isHCW()
    {
        if (IdmSessionProperties.getSessionInstance(mCtx).getUserType() == null)
            return false;
        if (IdmSessionProperties.getSessionInstance(mCtx).getUserType().equals("HCW"))
            return true;
        else
            return false;
    }

    @Override
    protected void finalize()  {
        try {
            super.finalize();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        Log.d("mBiometricClient","closing socket");
        if(mBiometricClient != null) mBiometricClient.killIfClientRunning();
    }

    /**
     *This method will parse given string value to Integer.
     * @param text (type String)
     * @return
     */
    public int parseStringToInteger(String text) {
        try {
            return Integer.parseInt(text.trim());
        } catch (NumberFormatException e) {
            return 0;
        }
    }
 }