package org.iprd.identity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dhi.identitymodule.BackgroundManager;
import com.dhi.identitymodule.IdentityManager;
import com.dhi.identitymodule.IdentityModule;
import com.dhi.identitymodule.IdentityUtils;
import com.dhi.identitymodule.SegmentationAndMatching;
import com.dhi.identitymodule.SessionProperties;
import com.dhi.identitymodule.AppType;
import com.dhi.identitymodule.BuildType;
import com.neurotec.lang.NCore;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.ACCESS_WIFI_STATE;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_SETTINGS;

/**
 * This is the landing page for identity manager, decide based on the request which page to be display.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class MainActivity extends AppCompatActivity {
    final int CONNECT = 1;
    final int ADD_PERSON = 2;
    final int HANDLE_POS = 3;
    private int HCW_TEST_CT = 1;
    private int PATIENT_TEST_CT = 1;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private SharedPreferences mSharedPreferences;
    public ProgressBar mCyclicProgressBar;
    TextView mIdmMain;

    int apiType = IdMDefinitions.NO_REQ;
    Map<String,String> apiParams = new HashMap<String,String>();
    JSONObject mResJSON = null;
    private Context mCtx;
    Intent data = new Intent();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(!checkpermission()){
            requestPermission();
            super.onCreate(savedInstanceState);
        }
        else {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            NCore.setContext(this);
            mCtx = getApplicationContext();
            AppType app = AppType.GENERAL_APP;
            BuildType build = BuildType.DEV;
            StringBuffer appStr = new StringBuffer();
            StringBuffer buildStr = new StringBuffer();

            if (getAppConfig(appStr, buildStr)) {
                if (appStr.toString().equals("GENERAL_APP"))
                    app = AppType.GENERAL_APP;
                else if (appStr.toString().equals("BIOMETRIC_ONLY"))
                    app = AppType.BIOMETRIC_ONLY;
                else
                    Toast.makeText(this,"Internal error: Please contact engineering team",Toast.LENGTH_LONG).show();
                if (buildStr.toString().equals("DEV"))
                    build = BuildType.DEV;
                else if (buildStr.toString().equals("RELEASE"))
                    build = BuildType.RELEASE;
                else
                    Toast.makeText(this,"Internal error: Please contact engineering team",Toast.LENGTH_LONG).show();
                IdmSessionProperties.getSessionInstance(mCtx).setAppFlag(app);
                IdmSessionProperties.getSessionInstance(mCtx).setBuildFlag(build);
            }
            else
                Toast.makeText(this,"Internal error: Please contact engineering team",Toast.LENGTH_LONG).show();
            IdentityManager.getIDMInstance().initPersistentStorage(mCtx);
            mCyclicProgressBar = (ProgressBar) findViewById(R.id.progressBar_cyclic);
            mIdmMain = (TextView) findViewById(R.id.idmMain);

            //create background manager thread to poll for location and replication
            //runs every 1 min
            BackgroundManager.getBackgroundThread(mCtx);
           // BackgroundManager backgroundManager = new BackgroundManager(mCtx);
           // backgroundManager.start();

            //SegmentationAndMatching sgM = SegmentationAndMatching.getInstance(mCtx);
            mSharedPreferences = getSharedPreferences("IdmSession", Context.MODE_PRIVATE);
            SharedPreferences.Editor edit = mSharedPreferences.edit();
            String identityrequest = getIdentityRequest();
            if (null == identityrequest) {
                Log.e("ID req error", "Null id req");
                ifIdentityRequestDataIsNull();
                return;
            }
            idnRequestInformation(identityrequest);
            Log.d("Debug log only", "About to call request");
            identityResponseData(edit);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            setContentView(R.layout.activity_main);
            NCore.setContext(this);
            mCtx = getApplicationContext();
            AppType app = AppType.GENERAL_APP;
            BuildType build = BuildType.DEV;
            StringBuffer appStr = new StringBuffer();
            StringBuffer buildStr = new StringBuffer();

            if (getAppConfig(appStr, buildStr)) {
                if (appStr.toString().equals("GENERAL_APP"))
                    app = AppType.GENERAL_APP;
                else if (appStr.toString().equals("BIOMETRIC_ONLY"))
                    app = AppType.BIOMETRIC_ONLY;
                if (buildStr.toString().equals("DEV"))
                    build = BuildType.DEV;
                else if (buildStr.toString().equals("RELEASE"))
                    build = BuildType.RELEASE;
                IdmSessionProperties.getSessionInstance(mCtx).setAppFlag(app);
                IdmSessionProperties.getSessionInstance(mCtx).setBuildFlag(build);
                if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.GENERAL_APP)
                    Log.d("App type after setting: ","General");
                else
                    Log.d("App type after setting:","Biometric");
            }
            if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.GENERAL_APP)
                Log.d("App type outside app config loop: ","General");
            else
                Log.d("App type outside app config loop:","Biometric");
            IdentityManager.getIDMInstance().initPersistentStorage(mCtx);
            mCyclicProgressBar = (ProgressBar) findViewById(R.id.progressBar_cyclic);
            mIdmMain = (TextView) findViewById(R.id.idmMain);

            //create background manager thread to poll for location
            BackgroundManager.getBackgroundThread(mCtx);

            //SegmentationAndMatching sgM = SegmentationAndMatching.getInstance(mCtx);
            mSharedPreferences = getSharedPreferences("IdmSession", Context.MODE_PRIVATE);
            SharedPreferences.Editor edit = mSharedPreferences.edit();
            String identityrequest = getIdentityRequest();
            if (null == identityrequest) {
                Log.e("ID req error", "Null id req");
                ifIdentityRequestDataIsNull();
                return;
            }
            idnRequestInformation(identityrequest);
            Log.d("Debug log only", "About to call request");
            identityResponseData(edit);
        }
        else
            Toast.makeText(this, "Permissions not enabled. Sorry!", Toast.LENGTH_LONG).show();
    }

    private boolean getAppConfig(StringBuffer app, StringBuffer build)
    {
        Map<String,String> cfgMap = new HashMap<String,String>();
        if(loadConfigMap(mCtx,cfgMap))
        {
            app.append(cfgMap.get("appType"));
            build.append(cfgMap.get("buildType"));
            Log.d("Apptype from map",app.toString());
            Log.d("Buildtype from map: ",build.toString());
        }
        else
            return false;
        return true;
    }


    private boolean loadConfigMap(Context ctx,Map<String,String> cfgMap)
    {
        String cfgMapStr = IdentityUtils.loadJSONFromAsset(ctx, R.string.app_config);
        if (cfgMapStr == null || cfgMapStr.length() == 0)
            return false;
        try {
            JSONObject cfgMapJSON = new JSONObject(cfgMapStr);
            String appType = cfgMapJSON.getJSONObject("idmConfig").getString("appType");
            String buildType = cfgMapJSON.getJSONObject("idmConfig").getString("buildType");
            Log.d("Apptype from json",appType);
            cfgMap.put("appType",appType);
            cfgMap.put("buildType",buildType);
            return true;
        } catch (JSONException e) {
            Log.e("Error while reading JSON",e.toString());
            return false;
        }
    }

    private void identityResponseData(SharedPreferences.Editor edit) {
        Intent data = new Intent();
        String resOut = "{ \"resourceType\":\"IdentityRequest\", \"reqTxt\":\"\", \"Outputs\":{ } }";

        try {
            JSONObject mResJSON = new JSONObject(resOut);
            if(apiType == CONNECT) {
                Log.d("API Type","Connect");
                StringBuffer ctx = new StringBuffer();
                StringBuffer api = new StringBuffer();
                if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.GENERAL_APP)
                    Log.d("App type: ","General");
                else
                    Log.d("App type","Biometric");
                IdentityManager.getIDMInstance().ConnectToIdentity(mCtx,ctx,api,IdmSessionProperties.getSessionInstance(mCtx).getAppFlag());
                IdmSessionProperties.getSessionInstance(mCtx).setSessionCtx(ctx.toString());
                if (mCyclicProgressBar != null) {
                    mCyclicProgressBar.setVisibility(ProgressBar.VISIBLE);
                    mIdmMain.setVisibility(View.VISIBLE);
                    mIdmMain.setText("Loading data...");
                }
                else
                    Log.e("Progress bar error","Null obj");
                DataLoaderTask dataLoader = new DataLoaderTask();
                dataLoader.execute();
                mResJSON.getJSONObject("Outputs").put("currCtx",ctx.toString());
                mResJSON.getJSONObject("Outputs").put("currAPI",api.toString());
                mResJSON.put("reqTxt","ConnectToIdentity");
                data.putExtra("IdentityResponse",mResJSON.toString());
                setResult(RESULT_OK, data);
                finish();
            } else if (apiType == ADD_PERSON) {
                StringBuffer guid = new StringBuffer();
                StringBuffer missingFields = new StringBuffer();
                if (mCyclicProgressBar != null) {
                    mCyclicProgressBar.setVisibility(ProgressBar.VISIBLE);
                    mIdmMain.setVisibility(View.VISIBLE);
                    mIdmMain.setText("Loading data...");
                }
                else
                    Log.e("Progress bar error","Null obj");
                handleUserLogin(guid,missingFields);

                if (apiParams.get("personType").equals("HCW"))
                    SessionProperties.getInstance(mCtx).setHcwName(apiParams.get("name"));
                else
                    edit.putString("patientGUID",guid.toString());
                edit.commit();
            } else if (apiType == HANDLE_POS) {
                handlePOS();
            } else {
                setResult(RESULT_CANCELED,data);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            data.putExtra("IdentityResponse",e.toString());
            setResult(RESULT_CANCELED,data);
            finish();
        }
    }


    private void ifIdentityRequestDataIsNull() {
        StringBuffer ctx = new StringBuffer();
        StringBuffer api = new StringBuffer();
        IdentityManager.getIDMInstance().ConnectToIdentity(mCtx,ctx,api,IdmSessionProperties.getSessionInstance(mCtx).getAppFlag());
        IdmSessionProperties.getSessionInstance(mCtx).setSessionCtx(ctx.toString());
        apiType = ADD_PERSON;
        apiParams.put("currCtx",ctx.toString());
        apiParams.put("currGUID",ctx.toString());
        apiParams.put("name",ctx.toString());
        apiParams.put("personType","HCW");

        StringBuffer guid = new StringBuffer();
        StringBuffer missingFields = new StringBuffer();
        handleUserLogin(guid,missingFields);
        return;
    }


    private void idnRequestInformation(String identityrequest) {
        try {
            JSONObject idmJSON = new JSONObject(identityrequest);
            String idmReqCode = idmJSON.getString("resourceType");
            String idmReq = idmJSON.getString("reqTxt");
            Log.d("IDMReqCode",idmReqCode);
            if ("IdentityRequest".equals(idmReqCode)) {
                if ("ConnectToIdentity".equals(idmReq)) {
                    apiType = CONNECT;
                    Toast.makeText(this, "IdM Connect req created", Toast.LENGTH_LONG).show();
                } else if (idmReq.equals("AddPerson")) {
                    apiType = ADD_PERSON;
                    Toast.makeText(this, "IdM Add person req created", Toast.LENGTH_LONG).show();
                    apiParams.put("currCtx",idmJSON.getJSONObject("Inputs").getString("currCtx"));
                    apiParams.put("personType",idmJSON.getJSONObject("Inputs").getString("personType"));
                    if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.BIOMETRIC_ONLY)
                        apiParams.put("userID",idmJSON.getJSONObject("Inputs").getString("userID"));
                } else if (idmReq.equals("HandlePOS")) {
                    apiType = HANDLE_POS;
                    apiParams.put("currCtx",idmJSON.getJSONObject("Inputs").getString("currCtx"));
                } else {
                    Toast.makeText(this, "Unknown IdM req", Toast.LENGTH_LONG).show();
                    setResult(RESULT_CANCELED);
                    finish();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(this,e.toString(),Toast.LENGTH_LONG).show();
            setResult(RESULT_CANCELED);
            finish();
        }
    }


    private String getIdentityRequest() {
        Intent intent = getIntent();
        String identityrequest = intent.getStringExtra("IdentityRequest");
        Log.d("IdM req",""+identityrequest);
        Toast.makeText(this,identityrequest,Toast.LENGTH_LONG).show();
        return identityrequest;
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    private void handlePOS() {
        String currCtx = apiParams.get("currCtx");
        Log.d("Ctx", currCtx);
        //here move to the Patient signature screen which should then take u to POS
        String resOut = "{ \"resourceType\":\"IdentityRequest\", \"reqTxt\":\"\", \"Outputs\":{ } }";

        try {
            mResJSON = new JSONObject(resOut);
            if (!IdentityManager.getIDMInstance().isValidContext(currCtx))
            {
                Log.e("POS-error","Invalid context");
                mResJSON.getJSONObject("Outputs").put("posDone","false");
                mResJSON.put("reqTxt","POS");
                data.putExtra("IdentityResponse",mResJSON.toString());
                setResult(RESULT_CANCELED,data);
                finish();
                return;
            }
            mResJSON.getJSONObject("Outputs").put("posDone","true");
            mResJSON.put("reqTxt","POS");
            data.putExtra("IdentityResponse",mResJSON.toString());
            setResult(RESULT_OK, data);
            Log.d("IdM","About to send JSON response");
        } catch (JSONException e) {
            data.putExtra("IdentityResponse",e.toString());
            setResult(RESULT_CANCELED,data);
            Log.e("IdM",e.toString());
            finish();
            return;
        }

        Intent i = new Intent(MainActivity.this,PatientSignature.class);
        i.putExtras(getIntent());
        i.putExtra("PSignature","true");
        startActivityForResult(i,IdMDefinitions.PSIG_INTENT_REQ);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data1) {
        if (resultCode == RESULT_OK)
        {
            Log.d("Main-actRes",Integer.toString(requestCode));
            if (requestCode == IdMDefinitions.PSIG_INTENT_REQ) {
                try {
                    Log.d("Main","Recvd ok from psig intent");
                    mResJSON.getJSONObject("Outputs").put("posDone", "true");
                    data.putExtra("IdentityResponse", mResJSON.toString());
                    setResult(RESULT_OK, data);
                    finish();
                } catch (JSONException e) {
                    Log.e("P Sig error",e.toString());
                    setResult(RESULT_CANCELED, data);
                    finish();
                }
            } else if (requestCode == IdMDefinitions.ADD_INTENT_REQ) {
                try {
                    Log.d("Main","Recvd ok from add intent");
                    mResJSON.getJSONObject("Outputs").put("addDone", "true");

                    //mResJSON.getJSONObject("Outputs").put("missingFields",missingFields);
                    if (apiParams.get("personType").equals("HCW")) {
                        if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.GENERAL_APP) {
                            mResJSON.getJSONObject("Outputs").put("name", SessionProperties.getInstance(mCtx).getHcwName());
                            mResJSON.getJSONObject("Outputs").put("dob",SessionProperties.getInstance(mCtx).getEpochDOB(true));
                        }
                        mResJSON.getJSONObject("Outputs").put("guid",SessionProperties.getInstance(mCtx).getHcwGUID());

                    }
                    else {
                        if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.GENERAL_APP) {
                            mResJSON.getJSONObject("Outputs").put("name", SessionProperties.getInstance(mCtx).getPatientName());
                            mResJSON.getJSONObject("Outputs").put("dob",SessionProperties.getInstance(mCtx).getEpochDOB(false));
                        }
                        mResJSON.getJSONObject("Outputs").put("guid",SessionProperties.getInstance(mCtx).getPatientGUID());

                    }
                    mResJSON.put("reqTxt","AddPerson");
                    data.putExtra("IdentityResponse", mResJSON.toString());
                    setResult(RESULT_OK, data);
                    finish();
                } catch (JSONException e) {
                    Log.e("Exception from intent call: ",e.toString());
                    e.printStackTrace();
                    setResult(RESULT_CANCELED, data);
                    finish();
                }
            }
        }
        else
        {
            Log.e("Intent call result","Canceled event");
            setResult(RESULT_CANCELED,data);
            finish();
        }
    }

    private void addTestData(StringBuffer ctx) {
        int hcwCt = 0;
        int patientCt = 0;

        if (IdentityModule.getInstance(mCtx).getNoOfUsers())
        {
            hcwCt = IdentityModule.getInstance(mCtx).getHCWCt();
            patientCt = IdentityModule.getInstance(mCtx).getPatientCt();
        }
        StringBuffer guid = new StringBuffer();
        StringBuffer newguid = new StringBuffer();
        StringBuffer missingParams = new StringBuffer();
        if (hcwCt < HCW_TEST_CT)
        {
            IdentityManager.getIDMInstance().AddPerson(ctx.toString(),null,mCtx,"Doctor","","",0,"91","7829246387","","Male","None","","HCW",null,"","","",newguid,missingParams);
        }
        if (patientCt < PATIENT_TEST_CT)
        {
            IdentityManager.getIDMInstance().AddPerson(ctx.toString(),null,mCtx,"Patient","","",0,"91","","","Male","None","","Patient",null,"","","",newguid,missingParams);
        }
    }

    /**
     * This method is used for handling add person request (HCW/Patient).
     * From here it makes an intant call to user wizard screen
     * @param guid
     * @param missingFields
     */

    public void handleUserLogin(StringBuffer guid, StringBuffer missingFields) {
        String currCtx = apiParams.get("currCtx");
        Log.d("Ctx", currCtx);
        if (!IdentityManager.getIDMInstance().isValidContext(currCtx))
        {
            Log.e("Error","Invalid context");
            setResult(RESULT_CANCELED,data);
            finish();
            return;
        }
        Intent i;

        if (apiParams.get("personType").equals("HCW")) {
      /*      SessionProperties.getInstance(mCtx).setHcwName(apiParams.get("name"));
            SessionProperties.getInstance(mCtx).setHCWPhone(apiParams.get("phone"));
            SessionProperties.getInstance(mCtx).setHCWCountryCode(apiParams.get("countryCode"));
            SessionProperties.getInstance(mCtx).setHCWDOB(apiParams.get("dob"));
            SessionProperties.getInstance(mCtx).setHcwGUID(guid.toString()); */
        } else if (apiParams.get("personType").equals("Patient")) {
        /*    SessionProperties.getInstance(mCtx).setPatientName(apiParams.get("name"));
            SessionProperties.getInstance(mCtx).setPatientPhone(apiParams.get("phone"));
            SessionProperties.getInstance(mCtx).setPatientCountryCode(apiParams.get("countryCode"));
            SessionProperties.getInstance(mCtx).setPatientDOB(apiParams.get("dob"));
            SessionProperties.getInstance(mCtx).setPatientGUID(guid.toString()); */
        } else {
            Log.e("Error","Invalid person type");
            setResult(RESULT_CANCELED,data);
            finish();
            return;
        }
        //now move to the Biometric screen - get the biometric data from the device (if present)
        String resOut = "{ \"resourceType\":\"IdentityRequest\", \"reqTxt\":\"\", \"Outputs\":{ } }";
        try {
            mResJSON = new JSONObject(resOut);
            mResJSON.getJSONObject("Outputs").put("addDone","false");
            mResJSON.getJSONObject("Outputs").put("guid",guid);
            if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.GENERAL_APP) {
                if (apiParams.get("personType").equals("HCW"))
                    mResJSON.getJSONObject("Outputs").put("name", SessionProperties.getInstance(mCtx).getHcwName());
                else
                    mResJSON.getJSONObject("Outputs").put("name", SessionProperties.getInstance(mCtx).getPatientName());
            }
            mResJSON.put("reqTxt","AddPerson");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (mCyclicProgressBar != null) {
            mCyclicProgressBar.setVisibility(ProgressBar.VISIBLE);
            mIdmMain.setVisibility(View.VISIBLE);
            mIdmMain.setText("Loading data...");
        }
        else
            Log.e("Progress bar error","Null obj");

        i = new Intent(MainActivity.this,UserInput.class);
        IdmSessionProperties.getSessionInstance(mCtx).setUserType(apiParams.get("personType"));
        if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.BIOMETRIC_ONLY)
            IdmSessionProperties.getSessionInstance(mCtx).setUserID(apiParams.get("userID"));
        i.putExtras(getIntent());
        i.putExtra("plhdEnum",UserDataType.USER_CONSENT);
        startActivityForResult(i, IdMDefinitions.ADD_INTENT_REQ);
        try {
            mResJSON = new JSONObject(resOut);
            mResJSON.getJSONObject("Outputs").put("addDone","false");
            mResJSON.getJSONObject("Outputs").put("guid",guid);
            mResJSON.getJSONObject("Outputs").put("missingFields",missingFields);
            if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.BIOMETRIC_ONLY)
                mResJSON.getJSONObject("Outputs").put("userID",IdmSessionProperties.getSessionInstance(mCtx).getUserID());
            if (IdmSessionProperties.getSessionInstance(mCtx).getAppFlag() == AppType.GENERAL_APP) {
                if (apiParams.get("personType").equals("HCW"))
                    mResJSON.getJSONObject("Outputs").put("name", SessionProperties.getInstance(mCtx).getHcwName());
                else
                    mResJSON.getJSONObject("Outputs").put("name", SessionProperties.getInstance(mCtx).getPatientName());
            }
            mResJSON.put("reqTxt","AddPerson");
        } catch (JSONException e) {
            Log.e("Exception: ",e.toString());
            e.printStackTrace();
        }
  }

        public void finish() {
            super.finish();
        }

        private class DataLoaderTask extends AsyncTask<String,String,String> {
            public DataLoaderTask() {
            }

            /**
             * Override this method to perform a computation on a background thread. The
             * specified parameters are the parameters passed to {@link #execute}
             * by the caller of this task.
             * <p>
             * This method can call {@link #publishProgress} to publish updates
             * on the UI thread.
             *
             * @param strings The parameters of the task.
             * @return A result, defined by the subclass of this task.
             * @see #onPreExecute()
             * @see #onPostExecute
             * @see #publishProgress
             */
            @Override
            protected String doInBackground(String... strings) {
                Log.d("Downloading data: ", "Wait");
                SegmentationAndMatching sgM = SegmentationAndMatching.getInstance(mCtx);
                IdentityModule idModule = IdentityModule.getInstance(mCtx);
                publishProgress(strings);
                return null;
            }


            /**
             * Runs on the UI thread before {@link #doInBackground}.             *
             * @see #onPostExecute
             * @see #doInBackground
             */
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            /**
             * <p>Runs on the UI thread after {@link #doInBackground}. The
             * specified result is the value returned by {@link #doInBackground}.</p>
             *
             * <p>This method won't be invoked if the task was cancelled.</p>
             *
             * @param s The result of the operation computed by {@link #doInBackground}.
             * @see #onPreExecute
             * @see #doInBackground
             */
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Log.d("Done downloading data: ","Thanks!");
            }

            /**
             * Runs on the UI thread after {@link #publishProgress} is invoked.
             * The specified values are the values passed to {@link #publishProgress}.
             *
             * @param values The values indicating progress.
             * @see #publishProgress
             * @see #doInBackground
             */
            @Override
            protected void onProgressUpdate(String... values) {
                super.onProgressUpdate(values);
                Log.i("Main-display: ","Downloaded data");
                mCyclicProgressBar.setVisibility(ProgressBar.INVISIBLE);
                mIdmMain.setText("Loaded all data");
                finish();
            }

            private void showProgressBar(final boolean b) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.i("Value of display flag: ",Boolean.toString(b));
                        mCyclicProgressBar = (ProgressBar) findViewById(R.id.progressBar_cyclic);
                        mCyclicProgressBar.setVisibility(b?View.VISIBLE:View.INVISIBLE);
                    }
                });
            }

        }

    private boolean checkpermission(){
        int res = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int res1 = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int res2 = ContextCompat.checkSelfPermission(getApplicationContext(), INTERNET);
        int res3 = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        int res4 = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_WIFI_STATE);
        int res5 = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_SETTINGS);
        int res6 = ContextCompat.checkSelfPermission(getApplicationContext(), READ_PHONE_STATE);
        int res7 = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);
        return res1 == PackageManager.PERMISSION_GRANTED && res == PackageManager.PERMISSION_GRANTED && res2 == PackageManager.PERMISSION_GRANTED && res3 == PackageManager.PERMISSION_GRANTED &&
                res4 == PackageManager.PERMISSION_GRANTED && res5 == PackageManager.PERMISSION_GRANTED && res6 == PackageManager.PERMISSION_GRANTED && res7 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission(){
        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE, CAMERA,ACCESS_FINE_LOCATION,INTERNET,WRITE_SETTINGS,ACCESS_WIFI_STATE,READ_PHONE_STATE,RECORD_AUDIO}, PERMISSION_REQUEST_CODE);
    }
}