package org.iprd.identity;

import android.content.Context;
import android.util.Log;

import com.neurotec.lang.NCore;
import com.neurotec.licensing.NLicense;
import com.neurotec.plugins.NDataFileManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

/**
 * This class represents the neurotec license installed on the device for biometric data detection
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class NeuroLicense {
    private static final String TAG = NeuroLicense.class.getName();
    private static Context mCtx;

    public NeuroLicense(Context c){
        mCtx=c;
    }

    /**
     * this method is used to convert Stream to String data.
     * @param is : input stream
     * @return
     * @throws IOException
     */
    public static String convertStreamToString(InputStream is) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        Boolean firstLine = true;
        while ((line = reader.readLine()) != null) {
            if(firstLine){
                sb.append(line);
                firstLine = false;
            } else {
                sb.append("\n").append(line);
            }
        }
        reader.close();
        return sb.toString();
    }

    /**
     * this method is used for getting the given string from File.
     * here, we use this to load license from a given path.
     * @param filePath
     * @return
     * @throws IOException
     */
    public static String getStringFromFile (String filePath) throws IOException {
        File fl = new File(filePath);
        FileInputStream fin = new FileInputStream(fl);
        String ret = convertStreamToString(fin);
        fin.close();
        return ret;
    }

    /**
     * this method returns a path to which we store content.
     * @return : external disk space path.
     */
    public static String LoadExternalDisk(){
        return "/mnt/sdcard";
    }

    /**
     * for loading license from file
     * @return
     */
    protected boolean loadLicenseFromfile() {
        boolean rt = false;
        String basedir=LoadExternalDisk();
        String licpath= basedir + "/Neurotechnology/Licenses";
        NDataFileManager.getInstance().addFromDirectory(basedir+"/Neurotechnology", false);
        File dir = new File(licpath);
        File[] licFiles = dir.listFiles();
        Log.i(TAG,Long.toString(licFiles.length));
        for(int i=0;i< licFiles.length;i++){
            String licPath=licpath+"/"+licFiles[i].getName();
            if(licPath.contains(".lic")) {
                try {
					Log.i(TAG,"Adding LIC file " + licPath);
                    String content = getStringFromFile(licPath);
                    NLicense.add(content);
                    Log.i(TAG,"LIC Content " + content);
                    rt = true;
                } catch (Exception ex) {
                    Log.e(TAG, "Add License Failed for file " + licPath + " " + ex.getMessage());
                }
            }
        }
        return rt;
    }


    /**
     * This method is used to check if the given component has a valid license for this device.
     * Sample component could be FingerClient or FingerMatcher.
     * @param server
     * @param port
     * @param components
     * @return
     */
     boolean obtainLic(String  server ,int port , String components){
        boolean rt = false;
        try{
            rt = NLicense.obtain(server, port, components);
        }catch (Exception ex){
            String message = "Failed to obtain licenses for components.\nError message: "+ ex.getMessage();
            Log.e(TAG,message);
        }
        return rt;
    }

    /**
     * This method checks if the license on the phone is valid
     * @return
     */
    public boolean licenseCheckWithServer(){
        boolean ret = false;
        String Components = "FingerClient,FingerMatcher";
        try {
            loadLicenseFromfile();
            List<String> items = Arrays.asList(Components.split("\\s*,\\s*"));
            for (int i = 0; i < items.size(); i++) {
                  ret = obtainLic("/local", 5000, items.get(i));
                  Log.i(TAG,(ret?"Successfully able to get Lic for ":"Unable to get lic for ") + items.get(i));
            }
            return ret;
        }
        catch (Exception ex) {
            Log.e(TAG,ex.getMessage());
        }
        return ret;
    }
}
