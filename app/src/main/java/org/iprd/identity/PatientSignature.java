package org.iprd.identity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.gesture.GestureOverlayView;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Toast;

import com.dhi.identitymodule.SessionProperties;

import java.io.File;
import java.io.FileOutputStream;

import static android.graphics.Color.BLACK;
/**
 * This page displays a screen to get the user signature and consent.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class PatientSignature extends AppCompatActivity {

    private static final int REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION = 1;

    private GestureOverlayView mGestureOverlayView = null;

    private Button mRedrawButton = null;

    CheckBox mDiagnosisConsentCheckBox, mTreatmentConsentCheckBox, mPlaceConsentCheckBox;
    Button mNxtButton;
    private Context mCtx;

    /**
     *
      * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCtx = getApplicationContext();
        Toast.makeText(this,"Location@POS: "+SessionProperties.getInstance(mCtx).getLocationStr(),Toast.LENGTH_LONG).show();

        final File photo = new File(Environment.getExternalStorageDirectory(),  getResources().getString(R.string.signPicName));
        photo.delete();

        Intent prevScreenIntent = getIntent();
        if (prevScreenIntent.getStringExtra("PSignature").equals("true"))
            Log.d("PSignature","Received intent");

        setContentView(R.layout.activity_patient_signature);
        mNxtButton = (Button) findViewById(R.id.patientSigNxtBtn);
        greyoutButtons( R.drawable.grey_rounded_background, false);


        mDiagnosisConsentCheckBox = (CheckBox) findViewById(R.id.diagnosisConsent);
        mDiagnosisConsentCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked ){
                    if(mTreatmentConsentCheckBox.isChecked()&& mPlaceConsentCheckBox.isChecked()) {
                        greyoutButtons( R.drawable.blue_rounded_background, true);
                    }

                }else{
                    greyoutButtons( R.drawable.grey_rounded_background, false);
                }
            }
        });
        mTreatmentConsentCheckBox = (CheckBox) findViewById(R.id.treatmentConsent);
        mTreatmentConsentCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked ){
                    if(mDiagnosisConsentCheckBox.isChecked()&& mPlaceConsentCheckBox.isChecked()) {
                        greyoutButtons( R.drawable.blue_rounded_background, true);
                    }

                }else{
                    greyoutButtons( R.drawable.grey_rounded_background, false);
                }

            }
        });
        mPlaceConsentCheckBox = (CheckBox) findViewById(R.id.placeConsent);
        mPlaceConsentCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked ){
                    if(mDiagnosisConsentCheckBox.isChecked()&& mTreatmentConsentCheckBox.isChecked()) {
                        greyoutButtons( R.drawable.blue_rounded_background, true);
                    }

                }else{
                    greyoutButtons( R.drawable.grey_rounded_background, false);
                }

            }
        });
        init();
        mGestureOverlayView.addOnGesturePerformedListener(new CustomGestureListener());
        mRedrawButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mGestureOverlayView.clear(false);
                mGestureOverlayView.setGestureColor(BLACK);
            }

        });

        final ImageButton bckButton = (ImageButton) findViewById(R.id.signBackBtn);
        bckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bckButton.setEnabled(false);
                finish();
            }
        });

        mNxtButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissionAndSaveSignature();
                Intent i = new Intent(
                        PatientSignature.this,
                        ProofOfServiceScreen.class);
                i.putExtras(getIntent());
                setResult(Activity.RESULT_OK);
                startActivityForResult(i,IdMDefinitions.PSIG_INTENT_REQ);
            }
        });
        ImageButton login = (ImageButton) findViewById(R.id.hcwLgnPatientSign);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(PatientSignature.this, MainActivity.class);
                SessionProperties.getInstance(mCtx).clearAll();
                finish();
                startActivity(i);
            }
        });

        ImageButton exitout = (ImageButton) findViewById(R.id.exitPatientSign);
        exitout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               onBackPressed();
            }
        });
        ImageButton plogin = (ImageButton) findViewById(R.id.patientLgnPatientSign);
        plogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent i = new Intent(PatientSignature.this,PatientEnroll.class);
                SessionProperties.getInstance(mCtx).clearPatient();
                finish();
                startActivity(i);*/
            }
        });
    }

    /**
     * onBackPressed() :  pop-up dialog for exit the MGD app or cancel the pop-up dialog box.
     */
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setTitle("Exiting MGD")
        .setMessage("Are you sure you want to exit this application?")
        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            /**
             *
              * @param dialog
             * @param which
             */
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SessionProperties.getInstance(mCtx).clearAll();
                finishAndRemoveTask();
                finish();
            }

        })
        .setNegativeButton("No", null)
        .show();
    }


    private void greyoutButtons(int p, boolean b) {
            mNxtButton.setBackground(getResources().getDrawable(p, null));
            mNxtButton.setEnabled(b);
    }

    /**
     * This method willl recieve the activity result and finishs the intant call.
     * @param requestCode
     * @param resultCode
     * @param data1
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data1) {
        if (resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }
        else{
            setResult(RESULT_CANCELED);
            finish();
        }
    }


    private void init()
    {
        if(mGestureOverlayView ==null){
            mGestureOverlayView = (GestureOverlayView)findViewById(R.id.sign_pad);
            mGestureOverlayView.setGestureColor(BLACK);
            mGestureOverlayView.setFadeEnabled(false);
            mGestureOverlayView.setFadeOffset(1000*60*60);
        }
        if(mRedrawButton ==null){
            mRedrawButton = (Button)findViewById(R.id.redraw_button);
        }
    }


    private void checkPermissionAndSaveSignature()
    {
        try {
            // Check whether this app has write external storage permission or not.
            int writeExternalStoragePermission = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

            // If do not grant write external storage permission.
            if(writeExternalStoragePermission!= PackageManager.PERMISSION_GRANTED)
            {
                // Request user to grant write external storage permission.
                ActivityCompat.requestPermissions(PatientSignature.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION);
            }else
            {
                saveSignature();
            }

        } catch (Exception e) {
            Log.v("Signature Gestures", e.getMessage());
            e.printStackTrace();
        }
    }


    private void saveSignature()
    {
        try {

            // First destroy cached image.
            mGestureOverlayView.destroyDrawingCache();

            // Enable drawing cache function.
            mGestureOverlayView.setDrawingCacheEnabled(true);

            // Get drawing cache bitmap.
            Bitmap drawingCacheBitmap = mGestureOverlayView.getDrawingCache();

            // Create a new bitmap
            //Bitmap bitmap = Bitmap.createBitmap(drawingCacheBitmap);
           // SessionProperties.getInstance(mCtx).setPhoto(SessionProperties.getInstance(mCtx).PatientSig_Pic,Bitmap.createBitmap(drawingCacheBitmap));

            // Get image file save path and name.
            File file = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.signPicName));

            file.createNewFile();

            FileOutputStream fileOutputStream = new FileOutputStream(file);
            drawingCacheBitmap.compress(Bitmap.CompressFormat.PNG, 85, fileOutputStream);

            // Flush bitmap to image file.
            fileOutputStream.flush();

            // Close the output stream.
            fileOutputStream.close();

            greyoutButtons( R.drawable.blue_rounded_background, true);

            //Toast.makeText(getApplicationContext(), "Signature file is saved to " + filePath, Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Log.v("Signature Gestures", e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     *
     *  This method will save the signature if write external storage permission is granted.
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION) {
            int grantResultsLength = grantResults.length;
            if (grantResultsLength > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                saveSignature();
            } else {
                Toast.makeText(getApplicationContext(), "You denied write external storage permission.", Toast.LENGTH_LONG).show();
            }
        }
    }
}