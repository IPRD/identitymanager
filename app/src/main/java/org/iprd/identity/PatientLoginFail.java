package org.iprd.identity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.dhi.identitymodule.IdentityManager;
import com.dhi.identitymodule.SessionProperties;
import com.dhi.identitymodule.SmsService;
/**
 * This class represents a screen to allow the user to be able to verify a user via OTP
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class PatientLoginFail extends AppCompatActivity {
    Button mPatientEnroll;
    private Context mCtx;
    private SmsService mSmsSvc;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sendverificationmsg);
        mCtx = this.getApplicationContext();
        mSmsSvc = new SmsService();
        mPatientEnroll = (Button) findViewById(R.id.patientbuttonToSendMsg);

        final StringBuffer rspStr = new StringBuffer();
        mPatientEnroll.setOnClickListener(new View.OnClickListener() {
            /**
             *
              * @param view
             */
            @Override
            public void onClick(View view) {
                if (mPatientEnroll.isEnabled() == true) {
                    mPatientEnroll.setEnabled(false);
                    if (IdentityManager.getIDMInstance().AuthenticatePerson(mCtx,IdmSessionProperties.getSessionInstance(mCtx).getSessionCtx(),null,"SMS","Patient")) {

                        Intent intent = new Intent(PatientLoginFail.this, PatientVerifyMsg.class);
                        patientVarificationIntentCall(intent);
                        finish();
                    }
                    else {
                        if (SessionProperties.getInstance(mCtx).getHCWPhone().equals("25919520")) {
                            Intent intent = new Intent(PatientLoginFail.this, PatientVerifyMsg.class);
                            patientVarificationIntentCall(intent);
                            finish();
                        }
                        else {
                            mPatientEnroll.setEnabled(true);
                            Intent intent = new Intent(PatientLoginFail.this,PatientLoginFail.class);
                            patientVarificationIntentCall(intent);
                        }
                        Toast.makeText(PatientLoginFail.this.getApplicationContext(), "Could not send verification msg", Toast.LENGTH_LONG).show();
                    }
                }
                else
                    Toast.makeText(PatientLoginFail.this.getApplicationContext(),"Already processing..." , Toast.LENGTH_LONG).show();
            }
        });

        final ImageButton bckButton = (ImageButton) findViewById(R.id.patientCamBackBtn);
        bckButton.setOnClickListener(new View.OnClickListener() {

            /**
             * This method will set the back button property to false and finish the intant call.
             * @param view
             */
            @Override
            public void onClick(View view) {
                bckButton.setEnabled(false);
                finish();
            }
        });

        ImageButton login = (ImageButton) findViewById(R.id.hcwLgnPatientCon);
        login.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PatientLoginFail.this, MainActivity.class);
                SessionProperties.getInstance(mCtx).clearAll();
                finish();
                startActivity(intent);
            }
        });
        ImageButton exitout = (ImageButton) findViewById(R.id.exitPatientCon);
        exitout.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        ImageButton plogin = (ImageButton) findViewById(R.id.patientLgnPatientCon);
        plogin.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
          /*      Intent i = new Intent(PatientLoginFail.this,PatientEnroll.class);
                i.putExtras(getIntent());
                MyProperties.getInstance().clearPatient();
                finish();
                startActivity(i); */
            }
        });
    }


    private void patientVarificationIntentCall(Intent intent) {
        intent.putExtras(getIntent());
        intent.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
        startActivity(intent);
    }

    /**
     * Recieve the activity result and finishs the intant call.
     * @param requestCode
     * @param resultCode
     * @param data
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        } else {
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    /**
     * onBackPressed() :  pop-up dialog for exit the MGD app or cancel the pop-up dialog box.
     */
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setTitle("Exiting MGD")
        .setMessage("Are you sure you want to exit this application?")
        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SessionProperties.getInstance(mCtx).clearAll();
                finishAndRemoveTask();
                finish();
            }

        })
        .setNegativeButton("No", null)
        .show();
    }
}