package org.iprd.identity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Environment;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * This class contains utility functions that are used in implementing the biometric recognition feature.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class BioUtils {
    /**
     * This static method will read the file from given file path.
     * This is used for reading finger print and iris.
     * @param fname : file name.
     * @return : byte[]
     */
    public static byte[] ReadFile(String fname){
        File file = new File(fname);
        if(file.exists()) {
            Log.e("file available", fname);
        }else{
            Log.e("file Not available", fname);
            return null;
        }

        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
            bufferedInputStream.read(bytes, 0, bytes.length);
            bufferedInputStream.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bytes;
    }

    /**
     *
     * This method will Read a file that contain a bio-metric image and generates a encoded string.
     * @param ctx
     * @param fname
     * @return : string
     */
    public static String ReadFileToBytes(Context ctx, String fname)  {
        if(fname == null) return "";
        byte[] imageBytes = ReadFile(fname);
        if(imageBytes == null) return "";
        String encodedImage = android.util.Base64.encodeToString(imageBytes, android.util.Base64.NO_WRAP);
        return encodedImage;
    }

    /**
     *
     * This method is used for reading a byte array and writing it to a file.
     * @param data
     * @param fname
     * @return : boolean
     */
    public static boolean WriteFile(byte[] data,String fname){
        boolean ret=false;
        File file = new File(fname);
        if(data == null) return false;
        try {
            FileOutputStream f = new FileOutputStream(file);
            f.write(data);
            f.close();
            ret = true;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ret = false;
        }
        return ret;
    }

    private static boolean WriteStringToBytesFile(Context ctx, String fname,String data)  {
        boolean ret=false;
        if(fname == null || data == null) return ret;
        try {
            byte[] img = android.util.Base64.decode(data, android.util.Base64.NO_WRAP);
            ret = WriteFile(img,fname);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ret = false;
        }

        return ret;
    }

    /**
     *  This method is used to read encoded strings for left eye, right eye and fingerprint and then store them in a JSON.
     *  It outputs a JSON string
     * @param ctx
     * @param leye
     * @param reye
     * @param fp
     * @return
     */
    public static String CreateTestMessage(Context ctx,String leye,String reye,String fp) {
        String ret = "";
        try {
            String l = ReadFileToBytes(ctx,leye);
            String r = ReadFileToBytes(ctx,reye);
            String f = ReadFileToBytes(ctx,fp);
            String tMessage = "{ \"eye\": { \"image\": { \"left\": \"\", \"right\": \"\" }, \"iris\": { \"left\": \"\", \"right\": \"\" } }, \"fingerPrint\": { \"right\": { \"thumb\": \"\", \"index\": \"\", \"middle\": \"\", \"ring\": \"\", \"pinky\": \"\" }, \"left\": { \"thumb\": \"\", \"index\": \"\", \"middle\": \"\", \"ring\": \"\", \"pinky\": \"\" }, \"template\": { \"right\": { \"thumb\": \"\", \"index\": \"\", \"middle\": \"\", \"ring\": \"\", \"pinky\": \"\" }, \"left\": { \"thumb\": \"\", \"index\": \"\", \"middle\": \"\", \"ring\": \"\", \"pinky\": \"\" } } } }";
            JSONObject resJSON = new JSONObject(tMessage);
            if(l.length() >0)resJSON.getJSONObject("eye").getJSONObject("image").put("left",l);
            if(r.length() >0)resJSON.getJSONObject("eye").getJSONObject("image").put("right",r);
            if(f.length() >0)resJSON.getJSONObject("fingerPrint").getJSONObject("right").put("index",f);
            ret = resJSON.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ret;
    }

    /**
     *  This method is used to read encoded strings for left eye, right eye and left and right fingerprint and then store them in a JSON.
     *  It outputs a JSON string
     * @param ctx
     * @param l
     * @param r
     * @param fl
     * @param fr
     * @return
     */
    public static String CreateImageMessage(Context ctx,String l,String r,String fl,String fr,String face) {
        String ret = "";
        try {
            String tMessage = "{ \"eye\": { \"image\": { \"left\": \"\", \"right\": \"\" }, \"iris\": { \"left\": \"\", \"right\": \"\" } }, \"fingerPrint\": { \"right\": { \"thumb\": \"\", \"index\": \"\", \"middle\": \"\", \"ring\": \"\", \"pinky\": \"\" }, \"left\": { \"thumb\": \"\", \"index\": \"\", \"middle\": \"\", \"ring\": \"\", \"pinky\": \"\" }, \"template\": { \"right\": { \"thumb\": \"\", \"index\": \"\", \"middle\": \"\", \"ring\": \"\", \"pinky\": \"\" }, \"left\": { \"thumb\": \"\", \"index\": \"\", \"middle\": \"\", \"ring\": \"\", \"pinky\": \"\" } } }, \"face\": { \"image\": \"\",\"template\": \"\"}}";
            JSONObject resJSON = new JSONObject(tMessage);
            if(l.length() >0)resJSON.getJSONObject("eye").getJSONObject("image").put("left",l);
            if(r.length() >0)resJSON.getJSONObject("eye").getJSONObject("image").put("right",r);
            if(fl.length() >0)resJSON.getJSONObject("fingerPrint").getJSONObject("left").put("index",fl);
            if(fr.length() >0)resJSON.getJSONObject("fingerPrint").getJSONObject("right").put("index",fr);
            if (face.length() > 0) resJSON.getJSONObject("face").put("image",face);
            ret = resJSON.toString();
        } catch (JSONException e) {
            Log.e("Issue while creating image: ",e.toString());
        }
        return ret;
    }

    /**
     * This method is used to get a specific json string from the biometric json doc.
     * Ex: we could use this to get the left eye image from the input JSON string.
     * Set the eyeORfinger variable to indicate if you want eye or finger image. Similarly, set leftORright to
     * indicate whether you want left or right image.
     * @param ctx
     * @param message
     * @param eyeORfinger
     * @param leftORright
     * @return : "".
     */
    public static String getImageStringFromJson(Context ctx, String message, boolean eyeORfinger, boolean leftORright) {
        try {
            JSONObject msgJSON = new JSONObject(message);
            JSONObject jsonObject= null;
            if(!eyeORfinger) {
                jsonObject = msgJSON.getJSONObject("fingerPrint").getJSONObject(leftORright ? "left" : "right");
                return jsonObject.getString("index");
            }else{
                jsonObject = msgJSON.getJSONObject("eye").getJSONObject("image");
                return jsonObject.getString(leftORright ? "left" : "right");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * This method will save the biometric encoded strings to a file.
     * @param ctx : Context.
     * @param leye : Left Eye
     * @param reye : Right Eye
     * @param fp : file path
     * @return : boolean
     */
    public static boolean SaveImages(Context ctx,String leye,String reye,String fp) {
        String ret = "";
        String p="/mnt/sdcard/Neurotechnology/";
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();
        String lname = p+"l_eye_"+ts+".png";
        boolean l = WriteStringToBytesFile(ctx, p+ts+"l_eye_"+".png",leye);
        boolean r = WriteStringToBytesFile(ctx, p+ts+"r_eye_"+".png",reye);
        boolean f = WriteStringToBytesFile(ctx, p+ts+"fp_"+".png",fp);
        return l|r|f;
    }

    /**
     * This method is used to read a biometric json and save the contents to a file.
     * Ex: from a biometic json string, we can save the eye, fingers to a file.
     * @param ctx : Context
     * @param message : JSON string
     */
    public static void SaveImagesFromJson(Context ctx,String message) {
        try {
            JSONObject msgJSON = new JSONObject(message);
            JSONObject rightHand = msgJSON.getJSONObject("fingerPrint").getJSONObject("right");
            JSONObject img = msgJSON.getJSONObject("eye").getJSONObject("image");
            BioUtils.SaveImages(ctx, img.getString("left"), img.getString("right"), rightHand.getString("index"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is used for re-sizing an image.
     * @param photoPath : file(image) path
     * @param targetW
     * @param targetH
     * @return : Bitmap
     */
    public static Bitmap resizeBitmap(String photoPath, int targetW, int targetH) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(photoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW/targetW, photoH/targetH);
        }
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        return BitmapFactory.decodeFile(photoPath, bmOptions);
    }

    /**
     * This method is used for reading a biometric image file and saving as JPEG image.
     * @param finalBitmap : bit map data
     * @param path : file path
     */
    public static void SaveJpegImage(Bitmap finalBitmap, String path) {

        File file = new File(path);
        if (file.exists()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is used for reading a biometric image file and saving as PNG image.
     * @param finalBitmap : final bit map
     * @param path : file path
     */
    public static  void SavePngImage(Bitmap finalBitmap, String path) {

        File file = new File(path);
        if (file.exists()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 80, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Find the day between given dates.
     * @param startDate
     * @param endDate
     * @return : long
     */
    public static long daysBetween(Calendar startDate, Calendar endDate) {
        long end = endDate.getTimeInMillis();
        long start = startDate.getTimeInMillis();
        return TimeUnit.MILLISECONDS.toDays(Math.abs(end - start));
    }

    /**
     * To convert days to age in years and months.
     * @param days : days in integer data
     * @return : int
     */
    public static int[] ConvertDaysToAge(int days){
        Calendar today = Calendar.getInstance();
        long end = today.getTimeInMillis();
        long d = days*24;
        d = d*3600;
        end = end - d*1000;

        Calendar dob = Calendar.getInstance();
        dob.setTimeInMillis(end);

        int yearsInBetween = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        int monthsDiff = today.get(Calendar.MONTH) - dob.get(Calendar.MONTH);

        return new int[] {yearsInBetween, monthsDiff};
    }
}