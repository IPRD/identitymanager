package org.iprd.identity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.dhi.identitymodule.IdentityManager;
import com.dhi.identitymodule.ImageUtils;
import com.dhi.identitymodule.SessionProperties;

import java.io.File;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.Manifest.permission_group.CAMERA;

/**
 * This class will launch the android camera application for HCW.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class HCWCamera extends AppCompatActivity {

    public final static String DEBUG_TAG = "HCWCamera";
    private static final int PERMISSION_REQUEST_CODE = 200;
    private static final int TAKE_PICTURE = 1;
    int orientation=0;
    Button mNextButton;
    Bitmap mCamImage = null;
    private Context mCtx;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCtx = getApplicationContext();
        if(!checkpermission()){
            requestPermission();
        }
        setContentView(R.layout.activity_hcwcamera);

        mNextButton = (Button) findViewById(R.id.hcwCamNextBtn);

        final File photo = new File(Environment.getExternalStorageDirectory(),  getResources().getString(R.string.HCWPicName));
        photo.delete();

        mNextButton.setBackground(getResources().getDrawable(R.drawable.grey_rounded_background,null));
        mNextButton.setEnabled(false);
        final ImageView image = (ImageView) findViewById(R.id.imageHCW);
        image.setImageResource(R.drawable.empty_profile);

        cameraCaptureButtonOnClicked(photo);

        final ImageButton bckButton = (ImageButton) findViewById(R.id.hcwcambckBtn);
        bckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bckButton.setEnabled(false);
                finish();
            }
        });

        nextButtonOnClicked();

        ImageButton login = (ImageButton) findViewById(R.id.hcwLgnHcwCam);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(HCWCamera.this, MainActivity.class);
                SessionProperties.getInstance(mCtx).clearAll();
                finish();
                startActivity(i);
            }
        });
        ImageButton exitout = (ImageButton) findViewById(R.id.exitHcwCam);

        exitout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               onBackPressed();
            }
        });
    }

    private void nextButtonOnClicked() {
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mNextButton.setEnabled(false);
                StringBuffer guid = new StringBuffer();
                StringBuffer missingParams = new StringBuffer();
                int addRet = IdentityManager.getIDMInstance().AddPerson(IdmSessionProperties.getSessionInstance(mCtx).getSessionCtx(), SessionProperties.getInstance(mCtx).getHcwGUID(),mCtx,"","","",0,"","","","","","","HCW", mCamImage,"","",IdmSessionProperties.getSessionInstance(mCtx).getmHcwBioJSON(),guid,missingParams);
                Log.d("HCWCam-AddPerson",Integer.toString(addRet));
                setResult(RESULT_OK);
                finish();
            }
        });
    }

    private void cameraCaptureButtonOnClicked(final File photo) {
        Button cameraCapture = (Button) findViewById(R.id.frontCaptureBtn);
        cameraCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
                String  camid = getFrontFacingCameraId(manager);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.putExtra("android.intent.extras.CAMERA_FACING",camid);
                Uri photoUri = FileProvider.getUriForFile(HCWCamera.this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        photo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(intent, TAKE_PICTURE);
            }
        });
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setTitle("Exiting MGD")
        .setMessage("Are you sure you want to exit this application?")
        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SessionProperties.getInstance(mCtx).clearAll();
                finishAndRemoveTask();
                finish();
            }

        })
        .setNegativeButton("No", null)
        .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        final ImageView image = (ImageView) findViewById(R.id.imageHCW);
        Log.d("HCWCam",Integer.toString(resultCode));
        Log.d("HCWCam",Integer.toString(requestCode));
        if (requestCode == TAKE_PICTURE && resultCode == Activity.RESULT_OK) {
            String path = Environment.getExternalStorageDirectory()+"/"+getResources().getString(R.string.HCWPicName);
            mCamImage = ImageUtils.readImageAndAlignExif(path);
            BioUtils.SaveJpegImage(mCamImage,path);
            mCamImage = BioUtils.resizeBitmap(path,720,1280);
            BioUtils.SaveJpegImage(mCamImage,path);
            image.setImageBitmap(mCamImage);
            mNextButton.setBackground(getResources().getDrawable(R.drawable.blue_rounded_background,null));
            mNextButton.setEnabled(true);
        } else if (resultCode == RESULT_OK && requestCode == IdMDefinitions.ADD_INTENT_REQ) {
            setResult(RESULT_OK);
            finish();
        } else  {
            setResult(RESULT_CANCELED);
            finish();
        }
    }


    private boolean checkpermission(){
        int res = ContextCompat.checkSelfPermission(mCtx, CAMERA);
        int res1 = ContextCompat.checkSelfPermission(HCWCamera.this, WRITE_EXTERNAL_STORAGE);
        return res1 == PackageManager.PERMISSION_GRANTED && res == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission(){
        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE, CAMERA}, PERMISSION_REQUEST_CODE);
    }

    private String getFrontFacingCameraId(CameraManager cManager) {
        String cameraId="0";
        try {
            cameraId=  cManager.getCameraIdList()[1];
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        return cameraId;
    }
}
