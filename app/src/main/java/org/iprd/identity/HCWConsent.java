package org.iprd.identity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;

import com.dhi.identitymodule.IdentityManager;
import com.dhi.identitymodule.SessionProperties;
/**
 * This class is used to display the consent screen for HCW's.
 * At this point, this functionality is integrated in UserInput.java.
 * However, we have retained this screen in the project
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class HCWConsent extends AppCompatActivity {
    CheckBox mHcwConsentCheckbox1, mHcwConsentCheckbox2;
    Button mNxtButton;
    private Context mCtx;
    String mConsentString = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hcwconsent);
        mNxtButton = (Button) findViewById(R.id.hcwCsntNextBtn);
        mNxtButton.setBackground(getResources().getDrawable(R.drawable.grey_rounded_background, null));
        mNxtButton.setEnabled(false);
        mCtx = this.getApplicationContext();

        final ImageButton bckButton = (ImageButton) findViewById(R.id.hcwconbckBtn);
        bckButton.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                bckButton.setEnabled(false);
                Intent i = new Intent(HCWConsent.this, HCWCamera.class);
                i.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                startActivity(i);
                finish();
            }
        });

        mHcwConsentCheckbox1 = (CheckBox) findViewById(R.id.hcwChkBox1);
        String t = getResources().getString(R.string.hcwcb1Name).toString();
        mConsentString = t.replaceFirst("-----", SessionProperties.getInstance(mCtx).getHcwName());
        mHcwConsentCheckbox1.setText((CharSequence) mConsentString);

        mHcwConsentCheckbox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            /**
             *
             * @param buttonView
             * @param isChecked
             */
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (mHcwConsentCheckbox2.isChecked()) {
                        mNxtButton.setBackground(getResources().getDrawable(R.drawable.blue_rounded_background, null));
                        mNxtButton.setEnabled(true);
                    }

                } else {
                    mNxtButton.setBackground(getResources().getDrawable(R.drawable.grey_rounded_background, null));
                    mNxtButton.setEnabled(false);
                }
            }
        });
        mHcwConsentCheckbox2 = (CheckBox) findViewById(R.id.hcwChkBox2);
        mHcwConsentCheckbox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            /**
             *
             * @param buttonView
             * @param isChecked
             */
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (mHcwConsentCheckbox1.isChecked()) {
                        mNxtButton.setBackground(getResources().getDrawable(R.drawable.blue_rounded_background, null));
                        mNxtButton.setEnabled(true);
                    }

                } else {
                    mNxtButton.setBackground(getResources().getDrawable(R.drawable.grey_rounded_background, null));
                    mNxtButton.setEnabled(false);
                }
            }
        });

        mNxtButton.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                //add this HCW consent transaction to backend DB
                //these are hardcoded values. pls make sure we populate these with actual values.
                mNxtButton.setEnabled(false);
                String patientGUID = "";
                String content = mConsentString;
                StringBuffer guid = new StringBuffer();
                StringBuffer missingParams = new StringBuffer();
                IdentityManager.getIDMInstance().AddPerson(IdmSessionProperties.getSessionInstance(mCtx).getSessionCtx(),SessionProperties.getInstance(mCtx).getHcwGUID(),getApplicationContext(),"", "","",0,"","","","","","","HCW",null, mConsentString,"",IdmSessionProperties.getSessionInstance(mCtx).getmHcwBioJSON(),guid,missingParams);
                Log.d("HCWConsent screen",""+SessionProperties.getInstance(mCtx).getHCWPhotoStr());
                setResult(RESULT_OK);
                finish();

            }
        });

        ImageButton login = (ImageButton) findViewById(R.id.hcwLgnHcwCon);
        login.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                Intent i = new Intent(HCWConsent.this, MainActivity.class);
                SessionProperties.getInstance(mCtx).clearAll();
                finish();
                startActivity(i);
            }
        });
        ImageButton exitout = (ImageButton) findViewById(R.id.exitHcwCon);
        exitout.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    /**
     * onBackPressed() :  pop-up dialog for exit the MGD app or cancel the pop-up dialog box.
     */
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setTitle("Exiting MGD")
        .setMessage("Are you sure you want to exit this application?")
        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            /**
             *
             * @param dialog
             * @param which
             */
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SessionProperties.getInstance(mCtx).clearAll();
                finishAndRemoveTask();
                finish();
            }

        })
        .setNegativeButton("No", null)
        .show();
    }

    /**
     * This will finish the current application call by intant call.
     */
    public void finish() {
       super.finish();
    }

}
