package org.iprd.identity;

import android.gesture.Gesture;
import android.gesture.GestureOverlayView;
import android.gesture.GestureOverlayView.OnGesturePerformedListener;
import android.support.v7.app.AppCompatActivity;

/**
 * This class is used to read the signature from the POS screen. Helps get the image (here of the signature) from the display.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class CustomGestureListener extends AppCompatActivity implements OnGesturePerformedListener {


    @Override
    public void onGesturePerformed(GestureOverlayView gestureOverlayView, Gesture gesture) {

    }
}