package org.iprd.identity;

import android.content.Context;
import android.util.Log;

import com.dhi.identitymodule.ClientScanResult;
import com.dhi.identitymodule.WifiClientList;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * Used to send a request for particular data and to read the responds. This class is the bridge for communacating between identity manager and bio-metric devices.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class BiometricManager {
    public String TAG="BiometricManager";
    private Socket mSocket=null;
    public static final int SERVERPORT = 3003;
    public static final String SERVER_IP = "192.168.43.1";
    private ClientThread mClientThread = null;
    private Thread mThread;
    private Context mCtx;
    private String mBiometricJSON;
    private static final String INIT_REQ = "Init";
    private static final String GET_REQUESTED_DATA = "GetData";
    private static final String OPEN_FP_REQ = "OpenFP";
    private static final String SCAN_FPR_REQ = "ScanFPR";
    private static final String SCAN_FPL_REQ = "ScanFPL";
    private static final String SCAN_IRIS_REQ = "ScanIris";
    private static final String GET_STATUS_REQ = "GetStatus";
    private static final String GET_IRIS_REQ = "GetDataE";
    private static final String GET_FPL_REQ = "GetDataFL";
    private static final String GET_FPR_REQ = "GetDataFR";
    private String mCurrentReq;
    private boolean mIsActiveConnection, mIsActiveFPSensor, mIsDataRecieved, mIsIrisSuccess, mIsFpSuccess;
    private boolean mShouldIQuit;
    private boolean mConnected=false;

    public BiometricManager(Context mCtx) {
        this.mCtx = mCtx;
        mCurrentReq = "";
        mIsActiveConnection = false;
        mIsActiveFPSensor = false;
        mIsIrisSuccess = false;
        mIsFpSuccess = false;
        mIsDataRecieved = false;
        mConnected=false;
    }

    /**
     * For creating client thread, for communicating bio-metric device.
     */
    public void createClientThread() {
        mClientThread = new ClientThread();
        mThread = new Thread(mClientThread);
        mThread.start();
        mShouldIQuit=false;
    }

    /**
     * This method will kill the thread if client thread is running.
     */
    public void killIfClientRunning(){
        if(mClientThread != null) {
            mClientThread.closeSocket();
            mConnected=false;
        }
    }

    /**
     * Whether thread should be quit, depends on parameter boolean value.
     * @param b
     */
    public void quitThread(boolean b){
        mShouldIQuit = b;
    }

    public boolean getQuitState(){
        return mShouldIQuit;
    }

    /**
     * This method will return the true/false depends on the connection establishment.
     * @return : boolean
     */
    public boolean isConnected(){ return mConnected;}

    /**
     * This method will process the biometric message given in parameter.
     * Get the request type and action accordingly.
     * @param str :given message parameter.
     */
    public void processBiometricMsg(String str) {
        Log.d("CurrentReq:", mCurrentReq);
        Log.d("ReqToBioDevice",str);
        switch (mCurrentReq)
        {
            case INIT_REQ:
                if (str.contains("Initialized"))
                    mIsActiveConnection = true;
                break;
            case GET_IRIS_REQ:
            case GET_FPL_REQ:
            case GET_FPR_REQ:
            case GET_REQUESTED_DATA:
                if (mIsActiveConnection == true) {
                    mBiometricJSON = str;
                    mIsDataRecieved = true;
                }
                break;
            case OPEN_FP_REQ:
            case SCAN_FPR_REQ:
            case SCAN_FPL_REQ:
            case SCAN_IRIS_REQ:
                Log.d("Open/Scan req: ",str);
                break;
            case GET_STATUS_REQ:
                Log.d("StatusRsp",str);
                String commaDelim = ";";
                String colonDelim = ":";
                String[] tokens = str.split(commaDelim);
                for (int i=0;i<tokens.length;i++)
                {
                    if (tokens[i].contains("Initialized"))
                    {
                        String[] toks = tokens[i].split(colonDelim);
                        if (Integer.parseInt(toks[1]) == 0)
                            mIsActiveConnection = false;
                        else
                            mIsActiveConnection = true;
                    }
                    if (tokens[i].contains("FpClosed"))
                    {
                        String[] toks = tokens[i].split(colonDelim);
                        if (Integer.parseInt(toks[1]) == 0)
                            mIsActiveFPSensor = true;
                        else
                            mIsActiveFPSensor = false;
                    }
                    if (tokens[i].contains("IrisStatus"))
                    {
                        String[] toks = tokens[i].split(colonDelim);
                        if ((toks.length > 1) &&toks[1].contains("SUCCESS"))
                            mIsIrisSuccess = true;
                        else
                            mIsIrisSuccess = false;
                    }
                    if (tokens[i].contains("FPStatus"))
                    {
                        String[] toks = tokens[i].split(colonDelim);
                        if ((toks.length > 1) &&toks[1].contains("SUCCESS"))
                            mIsFpSuccess = true;
                        else
                            mIsFpSuccess = false;
                    }
                }
                break;
            default:
                break;
        }
    }

    public boolean isFPSensorActive() {
        return (mIsActiveFPSensor && mIsActiveConnection);
    }

    public void setIrisCaptured(boolean b) {
        mIsIrisSuccess =b;
    }

    public void setFpCaptured(boolean b) {
        mIsFpSuccess =b;
    }

    public void setDataReceived(boolean b) {
        mIsDataRecieved =false;
    }

    public boolean isIrisCaptured() {
        return mIsIrisSuccess;
    }

    public boolean isFpCaptured() {
        return mIsFpSuccess;
    }

    boolean ismIsDataRecieved() {
        return mIsDataRecieved;
    }

    public String getmBiometricJSON() {
        return mBiometricJSON;
    }

    /**
     * Send the request to bio-metric device using client thread.
     * @param bioReq : String
     */
    public void sendRequestToBioDevice(String bioReq) {
        mClientThread.sendMessage(bioReq);
        mCurrentReq = bioReq;
        Log.d("We sent the request to biometric box",bioReq);
    }

    /**
     * This is used to communicate with bio-metric devices.
     * Date: Aug 30 2019
     * @author IPRD
     * @version 1.0
     */
    class ClientThread implements Runnable {
        private Socket socket;
        private BufferedReader input;

        @Override
        public void run() {
            WifiClientList wifiList = new WifiClientList(mCtx);
            while (!getQuitState()) {
                String serverIP = SERVER_IP;
                ArrayList<ClientScanResult> wifiClients = wifiList.getClientList(true);
                if (wifiClients != null && wifiClients.size() > 0) {
                    //this means there is a valid client available
                    Log.d("WifiClients", "wifi clients available");
                    serverIP = wifiClients.get(0).getIpAddr();
                    Log.d("CredenceIP", serverIP);
                }
               try {
                    InetAddress serverAddr = InetAddress.getByName(serverIP);
                    socket = new Socket(serverAddr, SERVERPORT);
                    mConnected=true;
                    sendMessage("ShowMessage:Connected to MGD Application");
                    while (!Thread.currentThread().isInterrupted()) {
                        this.input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                        Log.d(TAG, "Trying to Read Data from socket");
                        String message = input.readLine();
                        if (null == message || "Disconnect".contentEquals(message)) {
                            Thread.interrupted();
                            message = "Server Disconnected.";
                            //showMessage(message);
                            socket.close();
                            break;
                        }
                        Log.d("MsgFromBioDevice", message);
                        processBiometricMsg(message);
                    }
                } catch (UnknownHostException e1) {
                    //e1.printStackTrace();
                    Log.e("Socket server", "Unknown Host");
                } catch (IOException e1) {
                    //e1.printStackTrace();
                    Log.e("Socket server", "IOException");
                }
                mConnected=false;
                try {
                    mThread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        /**
         * This method is used for sending request over the client thread.
         * @param message : final String
         */
        void sendMessage(final String message) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (null != socket) {
                            PrintWriter out = new PrintWriter(new BufferedWriter(
                                    new OutputStreamWriter(socket.getOutputStream())),
                                    true);
                            out.println(message);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }

        /**
         * for closing the Socket.
         */
        void closeSocket() {
            try {
                quitThread(true);
                if(null != socket && socket.isConnected()) {
                    socket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
