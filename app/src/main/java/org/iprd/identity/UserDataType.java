package org.iprd.identity;
/**
 * This class represents the various states that the user input screen can be in
 * Depending on state, the buttons on the screen are added/removed.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public enum UserDataType {
    USER_CONSENT,
    USER_NAME,
    USER_AGE,
    USER_ID,
    GOV_ID,
    USER_EYE,
    USER_FINGER_R,
    USER_FINGER_L,
    USER_FACE,
    USER_PHONE,
    USER_PHONE_AUTH,
    USER_RESULT,
    NULL
}
