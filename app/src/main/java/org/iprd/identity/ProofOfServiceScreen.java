package org.iprd.identity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.dhi.identitymodule.IdentityManager;
import com.dhi.identitymodule.SessionProperties;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class represents the Proof of service screen.
 * This is displayed after the patient-HCW interaction is done. Here, the patient signature and transaction record is displayed and then stored in backend
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class ProofOfServiceScreen extends AppCompatActivity {
    private Context mCtx;
    String mImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCtx = this.getApplicationContext();
        Log.d("POS screen [HCW Photo]",""+SessionProperties.getInstance(mCtx).getHCWPhotoStr());
        Log.d("POS screen [Location]",""+SessionProperties.getInstance(mCtx).getLocationStr());
        setContentView(R.layout.activity_proof_of_service_screen);

        String hcwNameFromSession = SessionProperties.getInstance(getApplicationContext()).getHcwName();
        Log.d("hcwFromSession",hcwNameFromSession);

        Toast.makeText(this,"hcw name from session: "+hcwNameFromSession,Toast.LENGTH_LONG).show();

        final Button nxtButton = (Button) findViewById(R.id.proofOfConsentNextBtn);
        ImageView image = (ImageView) findViewById(R.id.imagePOS);

        StringBuffer posImageStringBuffer = new StringBuffer();
        Bitmap POS = IdentityManager.getIDMInstance().HandleProofOfService(mCtx,posImageStringBuffer);
        image.setImageBitmap(POS);
        mImage = posImageStringBuffer.toString();

        final ImageButton bckButton = (ImageButton) findViewById(R.id.posBackBtn);
        bckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bckButton.setEnabled(false);
                finish();
            }
        });
        nxtButton.setOnClickListener(new View.OnClickListener() {
            /**
             * Next button click events, this method will get the IDM instance and from IDM manager and from session property get the instance and finish the current intant call.
             * @param view
             */
            @Override
            public void onClick(View view) {
                nxtButton.setEnabled(false);
                String content = mImage;
                StringBuffer hcwguid = new StringBuffer();
                StringBuffer patientguid = new StringBuffer();
                StringBuffer missingParams = new StringBuffer();
                IdentityManager.getIDMInstance().AddPerson(IdmSessionProperties.getSessionInstance(mCtx).getSessionCtx(),SessionProperties.getInstance(mCtx).getHcwGUID(),getApplicationContext(),"","","",0,"","","","","","","HCW",null,"",content,IdmSessionProperties.getSessionInstance(mCtx).getmHcwBioJSON(),hcwguid,missingParams);
                IdentityManager.getIDMInstance().AddPerson(IdmSessionProperties.getSessionInstance(mCtx).getSessionCtx(),SessionProperties.getInstance(mCtx).getPatientGUID(),getApplicationContext(),"","","",0,"","","","","","","Patient",null,"",content,IdmSessionProperties.getSessionInstance(mCtx).getmPatientBioJSON(),patientguid,missingParams);
                SessionProperties.getInstance(mCtx).clearPatient();
                setResult(RESULT_OK);
                finish();
            }
        });

        ImageButton login = (ImageButton) findViewById(R.id.hcwLgnPatientPos);
        login.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view : anroid View Class object of present application.
             */
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ProofOfServiceScreen.this, MainActivity.class);
                SessionProperties.getInstance(mCtx).clearAll();
                finish();
                startActivity(i);
            }
        });
        ImageButton plogin = (ImageButton) findViewById(R.id.patientLgnPatientPos);
        plogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        ImageButton exitout = (ImageButton) findViewById(R.id.exitPatientPos);
        exitout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    /**
     *This method will create the alert dialog box and set the icon, title, messages show the dialog and finish as well as remove the MGD task
     */
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setTitle("Exiting MGD")
        .setMessage("Are you sure you want to exit this application?")
        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SessionProperties.getInstance(mCtx).clearAll();
                finishAndRemoveTask();
                finish();
            }

        })
        .setNegativeButton("No", null)
        .show();
    }

    /**
     * This method will set the identity request type and finish the super intant call.
     */
    public void finish() {
        String resOut = "{ \"resourceType\":\"IdentityRequest\", \"reqTxt\":\"\", \"Outputs\":{ } }";
        try {
            JSONObject resJSON = new JSONObject(resOut);
            resJSON.put("reqTxt","HandlePOS");
            Intent data = new Intent();
            data.putExtra("IdentityResponse",resJSON.toString());
            setResult(RESULT_OK, data);
            Log.d("idRspSuccess","Done sending rsp");

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("idRsp",e.toString());
            Toast.makeText(this,"Intent rsp: "+e.toString(),Toast.LENGTH_LONG).show();
        }
        super.finish();
    }
}