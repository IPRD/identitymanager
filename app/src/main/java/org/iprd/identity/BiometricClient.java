package org.iprd.identity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.dhi.identitymodule.ClientScanResult;
import com.dhi.identitymodule.SessionProperties;
import com.dhi.identitymodule.WifiClientList;

import java.util.HashMap;
import java.util.Map;

/**
 * Used to connect to a bio-metric server running on a credence id device.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class BiometricClient extends AppCompatActivity{

    public String TAG="BiometricClient";
    public static final int SERVERPORT = 3003;
    public static final String SERVER_IP = "192.168.43.1";
    private ClientThread mClientThread;
    private Thread mThread;
    private Handler handler;
    private Context mCtx;
    private String mBioJSON;
    private Button mHcwLoginBtn;
    Intent mSendIntent;
    private TextView mReceivedMessageView;
    private Button mScanNConnectBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biometricclient);

        mReceivedMessageView = (TextView) findViewById(R.id.ReceivedMessage);
        mReceivedMessageView.setMovementMethod(new ScrollingMovementMethod());
        mReceivedMessageView.setTextSize(11f);
        mScanNConnectBtn = (Button) findViewById(R.id.ScanAndConnect);
        handler = new Handler();
        mHcwLoginBtn = findViewById(R.id.hcwLoginBtn2);
        mBioJSON = "";
       //uncomment this line for testing ONLY.
        //Ex: if you want to test something with biometric data and the credence id biometrix box is not functional, use this to generate some data.
        //make sure u have the files mentioned in this folder ready.
        // mBioJSON = BioUtils.CreateTestMessage(mCtx,"/mnt/sdcard/Neurotechnology/leftIris.png","/sdcard/Neurotechnology/rightIris.png","/mnt/sdcard/Neurotechnology/fingerpPrint.png");

        mCtx = getApplicationContext();
        mHcwLoginBtn.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * For processing bio-metric json.
             * @param v : Anroid View Class Object.
             */
            @Override
            public void onClick(View v) {
                processBiometricJSON(mBioJSON);
            }
        });

        mScanNConnectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMessage("Connecting to Server...");
                mClientThread = new ClientThread();
                mThread = new Thread(mClientThread);
                mThread.start();
                showMessage("Connected to Server...");
            }
        });
    }

    /**
     * For displaying the given message(as parameter), will run on UI thread.
     * @param message
     */
    public void showMessage(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mReceivedMessageView.setText(message);
            }
        });
    }

    private void processBiometricJSON(String bioJSON) {
        mHcwLoginBtn.setEnabled(false);
        Log.d("bio-data recvd from box",bioJSON);
        //send the biometric data (recvd as json string) to add person
        String name,countryCode,phone,dob,gender,skill,location,consent,POS;
        boolean isHCW = true;
        if (IdmSessionProperties.getSessionInstance(mCtx).getUserType().equals("HCW")) {
            name = SessionProperties.getInstance(mCtx).getHcwName();
            countryCode = SessionProperties.getInstance(mCtx).getHCWCountryCode();
            phone = SessionProperties.getInstance(mCtx).getHCWPhone();
            dob = SessionProperties.getInstance(mCtx).getHCWDOB();
            gender = SessionProperties.getInstance(mCtx).getHCWGender();
            skill = SessionProperties.getInstance(mCtx).getHCWSkill();
            consent = SessionProperties.getInstance(mCtx).getHCWConsent();
            isHCW = true;
        }
        else {
            name = SessionProperties.getInstance(mCtx).getPatientName();
            countryCode = SessionProperties.getInstance(mCtx).getPatientCountryCode();
            phone = SessionProperties.getInstance(mCtx).getPatientPhone();
            dob = SessionProperties.getInstance(mCtx).getPatientDOB();
            gender = SessionProperties.getInstance(mCtx).getPatientGender();
            skill = SessionProperties.getInstance(mCtx).getPatientSkill();
            consent = SessionProperties.getInstance(mCtx).getPatientConsent();
            isHCW = false;
        }
        POS = SessionProperties.getInstance(mCtx).getPOS();
        location = SessionProperties.getInstance(mCtx).getLocationStr();
        StringBuffer guid = new StringBuffer();
        StringBuffer missingFields = new StringBuffer();
        Map<String,String> updateParams = new HashMap<String, String>();
        if (name != null && name.length() != 0)
            updateParams.put("name", name);
        if (phone != null && phone.length() != 0)
            updateParams.put("phone", phone);
        if (countryCode != null && countryCode.length() != 0)
            updateParams.put("countryCode", countryCode);
        if (dob != null && dob.length() != 0)
            updateParams.put("DOB", dob);
        updateParams.put("BiometricData",bioJSON);
        SessionProperties.getInstance(mCtx).updateMyPropertiesForUser(isHCW,updateParams);
        Intent i;
        if (IdmSessionProperties.getSessionInstance(mCtx).getUserType().equals("HCW")) {
            i = new Intent(BiometricClient.this, HCWLoginFail.class);
        } else {
            i = new Intent(BiometricClient.this, PatientLoginFail.class);
        }
        i.putExtras(getIntent());
        i.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
        startActivity(i);
        finish();
    }


    /**
     * Try to get all wifi clients, when the device setup as hotspot.
     * Date: Aug 30 2019
     * @author IPRD
     * @version 1.0
     */
    class ClientThread implements Runnable {

        private Socket socket;
        private BufferedReader input;

        @Override
        public void run() {

            try {
                String serverIP = SERVER_IP;
                WifiClientList wifiList = new WifiClientList(mCtx);

                ArrayList<ClientScanResult> wifiClients = wifiList.getClientList(true);

                if (wifiClients != null && wifiClients.size() > 0)
                {
                    //this means there is a valid client available
                    Log.d("WifiClients","wifi clients available");
                    serverIP = wifiClients.get(0).getIpAddr();
                    Log.d("CredenceIP",serverIP);
                }

                InetAddress serverAddr = InetAddress.getByName(serverIP);
                socket = new Socket(serverAddr, SERVERPORT);

                while (!Thread.currentThread().isInterrupted()) {
                    this.input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
					Log.d(TAG,"Trying to Read Data from socket");
                    String message = input.readLine();
                    if (null == message || "Disconnect".contentEquals(message)) {
                        Thread.interrupted();
                        message = "Server Disconnected.";
                        showMessage(message);
                        socket.close();
                        break;
                    }
                    //handle the json and get the biometric ids
                    mBioJSON = message;
                    if (IdmSessionProperties.getSessionInstance(mCtx).getUserType().equals("HCW"))
                        IdmSessionProperties.getSessionInstance(mCtx).setmHcwBioJSON(message);
                    else
                        IdmSessionProperties.getSessionInstance(mCtx).setmPatientBioJSON(message);

                    BioUtils.SaveImagesFromJson(mCtx,message);
                    showMessage("Received message from biometric server");
                }
            } catch (UnknownHostException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }

        void sendMessage(final String message) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (null != socket) {
                            PrintWriter out = new PrintWriter(new BufferedWriter(
                                    new OutputStreamWriter(socket.getOutputStream())),
                                    true);
                            out.println(message);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }

    }

    String getTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(new Date());
    }

    /**
     * For destroying the current thread.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != mClientThread) {
            mClientThread.sendMessage("Disconnect");
            mClientThread = null;
        }
    }
}
