package org.iprd.identity;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.widget.Toast;

import java.util.Locale;
/**
 * This class handles the text to speech functionality used in various IdM screens.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class SpeakOut {
    SpeakOut(Context c) {
        mCtx =c;
        mTTS = new TextToSpeech(c, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
            if (status == TextToSpeech.SUCCESS) {
                int ttsLang = mTTS.setLanguage(Locale.UK);

                if (ttsLang == TextToSpeech.LANG_MISSING_DATA
                        || ttsLang == TextToSpeech.LANG_NOT_SUPPORTED) {
                    Log.e("TTS", "The Language is not supported!");
                } else {
                    Log.i("TTS", "Language Supported.");
                }
                Log.i("TTS", "Initialization success.");
                mInitialized = true;
            } else {
                Toast.makeText(mCtx, "TTS Initialization failed!", Toast.LENGTH_SHORT).show();
            }
            }
        });
        mTTS.setSpeechRate(0.8f);
    }

    /**
     *This method will speak the given string parameter.
     * @param msg : String message to be speak
     */
    void Speak(String msg){
        Log.d("TTS",msg);
        if(mInitialized) {
            int speechStatus = mTTS.speak(msg, TextToSpeech.QUEUE_FLUSH, null);
            if (speechStatus == TextToSpeech.ERROR) {
                Log.e("TTS", "Error in converting Text to Speech!");
            }
        }
    }

    /**
     * This method will shot down the Speak out i.e, TextToSpeace class.
     */
    void Shutdown(){
        if (mTTS != null) {
            mTTS.stop();
            mTTS.shutdown();
        }
    }

    Context mCtx;
    TextToSpeech mTTS=null;
    boolean mInitialized=false;
}
